require("dotenv").config();
const express = require("express");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const app = express();
const csrf = require("csurf");

const ALLOW_ORIGIN = process.env.ALLOW_ORIGIN || "*";

var corsOptions = {
  origin: ALLOW_ORIGIN,
  credentials: true,
};

// middleware
app.use(cors(corsOptions));
app.use(express.json());
app.use(cookieParser());

const csrfProtection = csrf({ cookie: true });

// use CSRF protect, must below the cookieParser
// app.use(csrfProtection);

// routes
require("./app/routes/auth.routes")(app);
require("./app/routes/user.routes")(app);
require("./app/routes/data.routes")(app);
require("./app/routes/ftp.routes")(app);

const PORT = process.env.PORT || 9999;
// server
app.listen(PORT, () => {
  console.log(`server started on port ${PORT}`);
});
