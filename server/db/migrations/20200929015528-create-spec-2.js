"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "spec_2s",
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          type: Sequelize.INTEGER,
        },
        spec_code: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: "spec2_unique",
        },
        spec_var: {
          allowNull: false,
          type: Sequelize.STRING,
        },
        category_code: {
          allowNull: false,
          type: Sequelize.STRING,
          references: { model: "categories", key: "category_code" },
          unique: "spec2_unique",
          onDelete: "CASCADE",
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      },
      {
        uniqueKeys: {
          spec2_unique: {
            fields: ["spec_code", "category_code"],
          },
        },
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("spec_2s", {
      cascade: true,
    });
  },
};
