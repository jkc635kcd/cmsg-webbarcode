"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("categories", {
      id: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER,
      },
      category_code: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      category_name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      kind_code: {
        allowNull: false,
        type: Sequelize.STRING,
        references: { model: "kinds", key: "kind_code" },
        onDelete: "CASCADE",
      },
      discount: {
        allowNull: true,
        defaultValue: 1,
        type: Sequelize.FLOAT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("categories", {
      cascade: true,
    });
  },
};
