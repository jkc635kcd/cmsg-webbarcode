"use strict";
module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(
      'CREATE OR REPLACE VIEW barcode_view AS SELECT "kinds".kind_code, "categories".category_code, "spec_1s".spec_code AS spec_1_code, "spec_2s".spec_code AS spec_2_code,kind_name, category_name, "spec_1s".spec_var AS spec_1_var, "spec_2s".spec_var AS spec_2_var,(99|| "categories".category_code || "spec_1s".spec_code || "spec_2s".spec_code) AS barcode FROM "categories" INNER JOIN "spec_1s" ON "categories".category_code = "spec_1s".category_code INNER JOIN "spec_2s" ON "categories".category_code = "spec_2s".category_code INNER JOIN "kinds" ON "kinds".kind_code = "categories".kind_code ORDER BY barcode'
    );
    await queryInterface.sequelize.query(
      'CREATE OR REPLACE VIEW main_views AS SELECT DISTINCT kind_code, category_code, spec_1_code, spec_2_code, kind_name, category_name, spec_1_var, spec_2_var, barcode ,price FROM barcode_view NATURAL JOIN "barcodes" ORDER BY barcode'
    );
    await queryInterface.sequelize.query(
      "CREATE TABLE main_table AS SELECT * FROM main_views"
    );
    await queryInterface.sequelize.query(
      "CREATE TABLE spec_table AS SELECT kind_name, category_name, spec_1_var, spec_2_var FROM main_views"
    );
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(
      "DROP VIEW IF EXISTS barcode_view CASCADE"
    );
    await queryInterface.sequelize.query("DROP TABLE IF EXISTS main_table");
    await queryInterface.sequelize.query("DROP TABLE IF EXISTS spec_table");
  },
};
