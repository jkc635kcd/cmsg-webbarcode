"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("kinds", {
      id: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER,
      },
      kind_code: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      kind_name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      group_code: {
        allowNull: false,
        type: Sequelize.STRING,
        references: { model: "kindgroups", key: "group_code" },
        onDelete: "CASCADE",
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("kinds", {
      cascade: true,
    });
  },
};
