"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "spec_1s",
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          type: Sequelize.INTEGER,
        },
        spec_code: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: "spec1_unique",
        },
        spec_var: {
          allowNull: false,
          type: Sequelize.STRING,
        },
        category_code: {
          allowNull: false,
          type: Sequelize.STRING,
          references: { model: "categories", key: "category_code" },
          unique: "spec1_unique",
          onDelete: "CASCADE",
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      },
      {
        uniqueKeys: {
          spec1_unique: {
            fields: ["spec_code", "category_code"],
          },
        },
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("spec_1s", {
      cascade: true,
    });
  },
};
