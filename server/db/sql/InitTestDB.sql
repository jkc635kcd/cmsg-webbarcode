-- 刪除資料表
DROP TABLE Kinds
CASCADE;
DROP TABLE Categories
CASCADE;
DROP TABLE Specification1
CASCADE;
DROP TABLE Specification2
CASCADE;
DROP TABLE barcode_price;


-- 類別資料表
CREATE TABLE kinds
(
    kind_code VARCHAR(3) PRIMARY KEY,
    kind_name VARCHAR(20) NOT NULL
);

-- 種類資料表
CREATE TABLE categories
(
    category_code VARCHAR(3) PRIMARY KEY,
    category_name VARCHAR(10) NOT NULL,
    kind_code VARCHAR(3) NOT NULL,
    UNIQUE(kind_code,category_code),
    FOREIGN KEY (kind_code) REFERENCES kinds (kind_code) ON DELETE CASCADE
);

-- 規格資料表(x軸)
CREATE TABLE specification1
(
    spec_code_1 VARCHAR(2) PRIMARY KEY,
    spec_num_1 NUMERIC,
    category_code VARCHAR(3) NOT NULL,
    UNIQUE(category_code,spec_code_1),
    FOREIGN KEY (category_code) REFERENCES categories (category_code) ON DELETE CASCADE
);

-- 規格資料表(y軸)
CREATE TABLE specification2
(
    spec_code_2 VARCHAR(2) PRIMARY KEY,
    spec_num_2 NUMERIC,
    category_code VARCHAR(3) NOT NULL,
    UNIQUE(category_code,spec_code_2),
    FOREIGN KEY (category_code) REFERENCES categories (category_code) ON DELETE CASCADE
);

-- 條碼價格資料表
CREATE TABLE barcode_price
(
    barcode_id SERIAL PRIMARY KEY,
    barcode VARCHAR(12) UNIQUE,
    price NUMERIC,
    UNIQUE(barcode, price)
);
