-- create view
CREATE OR REPLACE VIEW "barcode_view"
AS
SELECT "kinds".kind_code, "categories".category_code, "spec_1s".spec_code AS spec_1_code, "spec_2s".spec_code AS spec_2_code,
    kind_name, category_name, "spec_1s".spec_var AS spec_1_var, "spec_2s".spec_var AS spec_2_var,
    ('99'|| "kinds".kind_code || "categories".category_code || "spec_1s".spec_code || "spec_2s".spec_code) AS barcode
FROM "categories"
    INNER JOIN "spec_1s"
    ON "categories".category_code = "spec_1s".category_code
    INNER JOIN "spec_2s"
    ON "categories".category_code = "spec_2s".category_code
    INNER JOIN "kinds"
    ON "kinds".kind_code = "categories".kind_code
ORDER BY barcode;

CREATE OR REPLACE VIEW "main_view" AS
SELECT DISTINCT kind_code, category_code, spec_1_code, spec_2_code, barcode, kind_name, category_name, spec_1_var, spec_2_var, price
FROM "barcode_view" NATURAL JOIN "Barcode" 
;

-- trigger
CREATE FUNCTION generateBarcode
() RETURNS TRIGGER AS $example_table$
BEGIN
    INSERT INTO "Barcode"
        (barcode)
    SELECT barcode
    FROM "barcode_view"
    WHERE barcode NOT IN (SELECT barcode
    FROM "Barcode");
    RETURN NEW;
END;
$example_table$ LANGUAGE plpgsql;

CREATE TRIGGER "generateBarcodeTrigger"
    AFTER
INSERT ON
"Spec_2s"
FOR
EACH
ROW
EXECUTE PROCEDURE generateBarcode
(); 