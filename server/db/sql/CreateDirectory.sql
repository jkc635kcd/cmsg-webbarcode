Create table "directory"
(
    "id" Serial NOT NULL,
    "root_id" Integer NOT NULL Default 0,
    "name" Varchar(20)NOT NULL ,
    "status" boolean Default 'true',
    "created" Timestamp Default current_timestamp,
    "modified" Timestamp Default current_timestamp,
    UNIQUE (id,root_id),
    PRIMARY KEY ("id")
    --    FOREIGN KEY (root_id) REFERENCES directory (id) ON DELETE CASCADE
);
INSERT INTO directory
    (id,root_id,name)
VALUES
    (0, 0, '/');
Alter table "directory" add  FOREIGN KEY (root_id) REFERENCES directory (id) ON DELETE CASCADE;
Create index "directory_index" on "directory" using btree
("id","root_id","name");