-- 建立全品項檢視表
CREATE OR REPLACE VIEW "barcode_view"
AS
SELECT kind_code, category_code, spec_code_1, spec_code_2, kind_name, category_name, spec_num_1, spec_num_2,
    ('99'|| kind_code || category_code || spec_code_1 || spec_code_2) AS barcode
FROM categories
    INNER JOIN spec_1
    ON categories.category_id = spec_1.category_id
    INNER JOIN spec_2
    ON categories.category_id = spec_2.category_id
    INNER JOIN kinds
    ON kinds.kind_id = categories.kind_id
ORDER BY barcode;

CREATE OR REPLACE VIEW "barcode_price_view" AS
SELECT barcode, kind_name, category_name, spec_num_1, spec_num_2, price
FROM "barcode_view" NATURAL JOIN barcode_price 
;

-- 測試資料
insert into Categories
    (kind_id ,category_code, category_name)
VALUES
    (1, '004', '螺絲種類4');

insert into Specification1
    (category_id ,spec_code_1 ,spec_num_1)
VALUES
    (10, '01', 3),
    (10, '02', 4),
    (10, '03', 5);

insert into Specification2
    (category_id ,spec_code_2 ,spec_num_2)
VALUES
    (10, '01', 333),
    (10, '02', 433),
    (10, '03', 533);

-- 建立觸發器-有新的資料插入時更新對應的barcode資料表

CREATE FUNCTION generateBarcode
() RETURNS TRIGGER AS $example_table$
BEGIN
    INSERT INTO barcode_price
        (barcode)
    SELECT barcode
    FROM "barcode_view"
    WHERE barcode NOT IN (SELECT barcode
    FROM barcode_price);
    RETURN NEW;
END;
$example_table$ LANGUAGE plpgsql;

CREATE TRIGGER "generateBarcodeTrigger"
    AFTER
INSERT ON
Specification2
FOR
EACH
ROW
EXECUTE PROCEDURE generateBarcode
(); 