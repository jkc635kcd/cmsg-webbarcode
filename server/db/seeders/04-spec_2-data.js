"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Spec_2s", [
      {
        spec_code: "01",
        spec_var: "30",
        category_code: "001001",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        spec_code: "02",
        spec_var: "40",
        category_code: "001001",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        spec_code: "03",
        spec_var: "50",
        category_code: "001001",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        spec_code: "01",
        spec_var: "301",
        category_code: "001002",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        spec_code: "02",
        spec_var: "401",
        category_code: "001002",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        spec_code: "03",
        spec_var: "501",
        category_code: "001002",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Spec_2s", null, {});
  },
};
