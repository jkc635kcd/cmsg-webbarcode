"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Categories", [
      {
        category_code: "001001",
        category_name: "十字螺絲",
        kind_code: "001",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        category_code: "001002",
        category_name: "一字螺絲",
        kind_code: "001",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        category_code: "001003",
        category_name: "六角螺絲",
        kind_code: "001",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Categories", null, {});
  },
};
