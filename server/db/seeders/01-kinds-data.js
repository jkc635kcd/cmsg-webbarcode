"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Kinds", [
      {
        kind_code: "001",
        kind_name: "螺絲",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        kind_code: "002",
        kind_name: "水管",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        kind_code: "003",
        kind_name: "刀片",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Kinds", null, {});
  },
};
