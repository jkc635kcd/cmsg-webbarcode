"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Spec_1s", [
      {
        spec_code: "01",
        spec_var: "3",
        category_code: "001001",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        spec_code: "02",
        spec_var: "4",
        category_code: "001001",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        spec_code: "03",
        spec_var: "5",
        category_code: "001001",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        spec_code: "01",
        spec_var: "31",
        category_code: "001002",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        spec_code: "02",
        spec_var: "41",
        category_code: "001002",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        spec_code: "03",
        spec_var: "51",
        category_code: "001002",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Spec_1s", null, {});
  },
};
