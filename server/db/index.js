const { Sequelize, sequelize } = require("./models");
const { Parser } = require("json2csv");
const Kinds = require("./models").kinds;
const Categories = require("./models").categories;
const Spec_1 = require("./models").spec_1;
const Spec_2 = require("./models").spec_2;
const Spec_name = require("./models").name_spec;
const Barcode = require("./models").barcodes;
const MainView = require("./models").main_view;
const Kindgroups = require("./models").kindgroups;
const Roles = require("./models").roles;
const Op = Sequelize.Op;

// 新增類別群組
const CreateData_kindgroups = async (group_code, group_name) => {
  console.log(`Create kindgroup Data: ${group_code},${group_name}`);
  try {
    await Kindgroups.create({
      group_code: group_code,
      group_name: group_name,
    });
    return {
      status: 200,
      message: `Create kindgroup Data: ${group_code},${group_name}`,
    };
  } catch (error) {
    console.error(error["errors"][0]["message"]);
    return { status: 510, message: error["errors"][0]["message"] };
  }
};

// 刪除類別群組資料
const DestroyData_groups = async (group_code) => {
  console.log(`Destroy groups Data - group_code:${group_code}`);
  try {
    await Kindgroups.findAll({
      where: { group_code: group_code },
      rejectOnEmpty: true,
    });
    await Kindgroups.destroy({ where: { group_code: group_code } });
    return {
      status: 200,
      message: `Destroy groups Data - group_code:${group_code}`,
    };
  } catch (error) {
    console.error(error.name);
    return { status: 500, message: error.name };
  }
};

// 新增類別資料
const CreateData_kinds = async (kind_code, kind_name) => {
  console.log(`Create kinds Data: ${kind_code},${kind_name}`);
  const group_code = kind_code.substring(0, 1);
  try {
    await Kinds.create({
      kind_code: kind_code,
      kind_name: kind_name,
      group_code: group_code,
    });
    return {
      status: 200,
      message: `Create kinds Data: ${kind_code},${kind_name}`,
    };
  } catch (error) {
    console.error(error["errors"][0]["message"]);
    return { status: 510, message: error["errors"][0]["message"] };
  }
};

// 刪除類別資料
const DestroyData_kinds = async (kind_code) => {
  console.log(`Destroy kinds Data - kind_code:${kind_code}`);
  try {
    await Kinds.findAll({
      where: { kind_code: kind_code },
      rejectOnEmpty: true,
    });
    await Kinds.destroy({ where: { kind_code: kind_code } });
    return {
      status: 200,
      message: `Destroy kinds Data - kind_code:${kind_code}`,
    };
  } catch (error) {
    console.error(error.name);
    return { status: 500, message: error.name };
  }
};

// 新增種類資料
const CreateData_categories = async (
  category_code,
  category_name,
  spec_1_name,
  spec_2_name
) => {
  console.log(`Create categories Data: ${category_code},${category_name}`);
  const kind_code = category_code.substring(0, 3);
  try {
    await Categories.create({
      category_code: category_code,
      category_name: category_name,
      kind_code: kind_code,
    });
    await Spec_name.create({
      category_code: category_code + "s1",
      name: spec_1_name,
    });
    await Spec_name.create({
      category_code: category_code + "s2",
      name: spec_2_name,
    });
    return {
      status: 200,
      message: `Create categories Data: ${category_code},${category_name}`,
    };
  } catch (error) {
    console.error(error["errors"][0]["message"]);
    return { status: 510, message: error["errors"][0]["message"] };
  }
};

// 刪除種類資料
const DestroyData_categories = async (category_code) => {
  console.log(`Destroy categories Data - category_code:${category_code}`);
  try {
    await Categories.findAll({
      where: { category_code: category_code },
      rejectOnEmpty: true,
    });
    await Categories.destroy({ where: { category_code: category_code } });
    await Spec_name.destroy({ where: { category_code: category_code + "s1" } });
    await Spec_name.destroy({ where: { category_code: category_code + "s2" } });
    return {
      status: 200,
      message: `Destroy categories Data - category_code:${category_code}`,
    };
  } catch (error) {
    console.error(error.name);
    return { status: 500, message: error.name };
  }
};

// 新增物品規格(1)
const CreateData_spec_1 = async (spec_code, spec_var, category_code) => {
  console.log(`Create spec_1 Data: ${spec_code},${spec_var},${category_code}`);
  try {
    await Spec_1.create({
      spec_code: spec_code,
      spec_var: spec_var,
      category_code: category_code,
    });
    return {
      status: 200,
      message: `Create spec_1 Data: ${spec_code},${spec_var},${category_code}`,
    };
  } catch (error) {
    console.error(error["errors"][0]["message"]);
    return { status: 510, message: `spec_1: ${error["errors"][0]["message"]}` };
  }
};

// 新增物品規格(2)
const CreateData_spec_2 = async (spec_code, spec_var, category_code) => {
  console.log(`Create spec_2 Data: ${spec_code},${spec_var},${category_code}`);
  try {
    await Spec_2.create({
      spec_code: spec_code,
      spec_var: spec_var,
      category_code: category_code,
    });
    return {
      status: 200,
      message: `Create spec_2 Data: ${spec_code},${spec_var},${category_code}`,
    };
  } catch (error) {
    console.error(error["errors"][0]["message"]);
    return { status: 510, message: `spec_2: ${error["errors"][0]["message"]}` };
  }
};

// 刪除物品規格(一)
const DestroyData_spec_1 = async (category_code, spec_code) => {
  try {
    await Spec_1.findAll({
      where: { category_code: category_code, spec_code: spec_code },
      rejectOnEmpty: true,
    });
    await Spec_1.destroy({
      where: { category_code: category_code, spec_code: spec_code },
    });
    await Barcode.destroy({
      where: { barcode: { [Op.like]: `99${category_code}${spec_code}%` } },
    });
    return {
      status: 200,
      message: `Destroy spec_1 Data - category_code:${category_code}, spec_code:${spec_code}`,
    };
  } catch (error) {
    console.error(error.name);
    return { status: 500, message: error.name };
  }
};

// 刪除物品規格(二)
const DestroyData_spec_2 = async (category_code, spec_code) => {
  try {
    await Spec_2.findAll({
      where: { category_code: category_code, spec_code: spec_code },
      rejectOnEmpty: true,
    });
    await Spec_2.destroy({
      where: { category_code: category_code, spec_code: spec_code },
    });
    await Barcode.destroy({
      where: { barcode: { [Op.like]: `99${category_code}%${spec_code}` } },
    });
    return {
      status: 200,
      message: `Destroy spec_2 Data - category_code:${category_code}, spec_code:${spec_code}`,
    };
  } catch (error) {
    console.error(error.name);
    return { status: 500, message: error.name };
  }
};

// 更新類別群組資料
const UpdateData_groups = async (props) => {
  try {
    const res = await Kindgroups.update(
      {
        group_name: props.group_name,
        updatedAt: new Date(),
      },
      {
        where: { group_code: props.group_code },
      }
    );
    if (res && res == 1) {
      return {
        status: 200,
        message: `Update group - code: ${props.group_code}, name: ${props.group_name}`,
      };
    } else {
      return {
        status: 500,
        message: `Update group failed - code: ${props.group_code}`,
      };
    }
  } catch (error) {
    console.error(error.message);
    return { status: 500, message: error.message };
  }
};

// 更新類別資料
const UpdateData_kinds = async (props) => {
  try {
    const res = await Kinds.update(
      {
        kind_name: props.kind_name,
        updatedAt: new Date(),
      },
      {
        where: { kind_code: props.kind_code },
      }
    );
    if (res && res == 1) {
      return {
        status: 200,
        message: `Update kind - code: ${props.kind_code}, name: ${props.kind_name}`,
      };
    } else {
      return {
        status: 500,
        message: `Update kind failed - code: ${props.kind_code}`,
      };
    }
  } catch (error) {
    console.error(error.message);
    return { status: 500, message: error.message };
  }
};

// 更新種類資料
const UpdateData_categories = async (props) => {
  try {
    const res = await Categories.update(
      {
        category_name: props.category_name,
        discount: props.discount,
        updatedAt: new Date(),
      },
      { where: { category_code: props.category_code } }
    );
    if (res && res == 1) {
      return {
        status: 200,
        message: `Update category - code: ${props.category_code}, name: ${props.category_name}, discount: ${props.discount}`,
      };
    } else {
      return {
        status: 500,
        message: `Update category failed - code: ${props.category_code}`,
      };
    }
  } catch (error) {
    console.error(error.message);
    return { status: 500, message: error.message };
  }
};

// 更新規格(一)資料
const UpdateData_spec_1 = async (props) => {
  try {
    const res = await Spec_1.update(
      {
        spec_var: props.spec_var,
        updatedAt: new Date(),
      },
      {
        where: {
          [Op.and]: [
            { spec_code: props.spec_code },
            { category_code: props.category_code },
          ],
        },
      }
    );
    if (res && res == 1) {
      return {
        status: 200,
        message: `Update spec_1 - code: ${props.spec_code}, name: ${props.spec_var}`,
      };
    } else {
      return {
        status: 500,
        message: `Update spec failed - code: ${props.spec_code}`,
      };
    }
  } catch (error) {
    console.error(error.message);
    return { status: 500, message: error.message };
  }
};

// 更新規格(二)資料
const UpdateData_spec_2 = async (props) => {
  try {
    const res = await Spec_2.update(
      {
        spec_var: props.spec_var,
        updatedAt: new Date(),
      },
      {
        where: {
          [Op.and]: [
            { spec_code: props.spec_code },
            { category_code: props.category_code },
          ],
        },
      }
    );
    if (res && res == 1) {
      return {
        status: 200,
        message: `Update spec_2 - code: ${props.spec_code}, name: ${props.spec_var}`,
      };
    } else {
      return {
        status: 500,
        message: `Update spec failed - code: ${props.spec_code}`,
      };
    }
  } catch (error) {
    console.error(error.message);
    return { status: 500, message: error.message };
  }
};

const UpdateData_spec_1_name = async (props) => {
  try {
    const res = await Spec_name.update(
      {
        name: props.name,
        updatedAt: new Date(),
      },
      {
        where: {
          category_code: `${props.category_code}s1`,
        },
      }
    );
    if (res && res == 1) {
      return {
        status: 200,
        message: `Update spec_1_name - code: ${props.category_code}, name: ${props.name}`,
      };
    } else {
      return {
        status: 500,
        message: `Update spec_1_name failed - code: ${props.category_code}`,
      };
    }
  } catch (error) {
    console.error(error.message);
    return { status: 500, message: error.message };
  }
};

const UpdateData_spec_2_name = async (props) => {
  try {
    const res = await Spec_name.update(
      {
        name: props.name,
        updatedAt: new Date(),
      },
      {
        where: {
          category_code: `${props.category_code}s2`,
        },
      }
    );
    if (res && res == 1) {
      return {
        status: 200,
        message: `Update spec_2_name - code: ${props.category_code}, name: ${props.name}`,
      };
    } else {
      return {
        status: 500,
        message: `Update spec_2_name failed - code: ${props.category_code}`,
      };
    }
  } catch (error) {
    console.error(error.message);
    return { status: 500, message: error.message };
  }
};

// 更新條碼價格
const UpdateData_barcode = async (barcode, price, discount) => {
  console.log(`Update barcode price: ${barcode},${price},${discount}`);

  try {
    await Barcode.update(
      {
        price: price,
        discount: discount,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      { where: { barcode: barcode } }
    );
    return {
      status: 200,
      message: `Update barcode price: ${barcode},${price},${discount}`,
    };
  } catch (error) {
    console.error(error.message);
    return { status: 500, message: error.message };
  }
};

const UpdateData_specName = async (category_code, name) => {
  try {
  } catch (error) {
    console.error(error.message);
  }
};

const TruncateData = async () => {
  console.log("Truncate all table");
  try {
    await sequelize
      .transaction((t) => {
        var options = { raw: true, transcation: t };
        sequelize
          .query("TRUNCATE kinds RESTART IDENTITY CASCADE", null, options)
          .then(() => {
            return sequelize.query(
              "TRUNCATE categories RESTART IDENTITY CASCADE",
              null,
              options
            );
          })
          .then(() => {
            return sequelize.query(
              "TRUNCATE spec_1s RESTART IDENTITY CASCADE",
              null,
              options
            );
          })
          .then(() => {
            return sequelize.query(
              "TRUNCATE spec_2s RESTART IDENTITY CASCADE",
              null,
              options
            );
          })
          .then(() => {
            return sequelize.query(
              "TRUNCATE name_specs RESTART IDENTITY CASCADE",
              null,
              options
            );
          })
          .then(() => {
            return sequelize.query(
              "TRUNCATE kindgroups RESTART IDENTITY CASCADE",
              null,
              options
            );
          });
      })
      .success(() => {
        return "Truncate success";
      });
  } catch (error) {
    console.error(error.message);
    return error.message;
  }
};

// 取得品項資料
const SelectData = async (props) => {
  let data = "";
  let page = undefined;
  let pagination = undefined;
  try {
    switch (props.target) {
      // 所有類別群組
      case "groups":
        const groups = await Kindgroups.findAll({
          attributes: ["group_code", "group_name"],
          order: ["group_code"],
        });
        data = JSON.stringify(groups, null, 2);
        break;

      // 所有類別
      case "kinds":
        const kinds = await Kinds.findAll({
          attributes: ["kind_code", "kind_name"],
          where: { group_code: props.group_code },
          order: ["kind_code"],
        });
        data = JSON.stringify(kinds, null, 2);
        break;

      // 所有種類
      case "categories":
        const categories = await Categories.findAll({
          attributes: [
            "kind_code",
            "category_code",
            "category_name",
            "createdAt",
            "updatedAt",
          ],
          order: ["category_code"],
        });
        data = JSON.stringify(categories, null, 2);
        break;

      // 單一種類
      case "category":
        const category = await Categories.findAll({
          attributes: ["category_code", "category_name", "discount"],
          where: { kind_code: props.kind_code },
          order: ["category_code"],
        });
        data = JSON.stringify(category, null, 2);
        break;
      // 單一種類的所有規格
      case "spec":
        const spec = await MainView.findAll({
          attributes: [
            "category_code",
            "spec_1_code",
            "spec_2_code",
            "kind_name",
            "category_name",
            "spec_1_var",
            "spec_2_var",
          ],
          where: { category_code: props.category_code },
          order: ["category_code"],
        });
        data = JSON.stringify(spec, null, 2);
        break;
      // 單一種類的規格(一)
      case "spec_1":
        page = props.page - 1;
        pagination = props.pagination;
        const spec_1 = await Spec_1.findAndCountAll({
          attributes: ["spec_code", "spec_var"],
          where: { category_code: props.category_code },
          order: ["spec_code"],
          offset: pagination * page,
          limit: pagination,
        });
        data = JSON.stringify(spec_1, null, 2);
        break;
      // 單一種類的規格(二)
      case "spec_2":
        const spec_2 = await Spec_2.findAll({
          attributes: ["spec_code", "spec_var"],
          where: { category_code: props.category_code },
          order: ["spec_code"],
        });
        data = JSON.stringify(spec_2, null, 2);
        break;
      // 單一種類所有條碼
      case "barcodes_all":
        const barcodes_all = await Barcode.findAll({
          attributes: ["barcode", "price", "discount"],
          where: {
            barcode: { [Op.like]: `99${props.category_code}%` },
          },
          order: ["barcode"],
        });
        data = JSON.stringify(barcodes_all, null, 2);
        break;
      // 單一種類分頁條碼
      case "barcodes":
        let barcode_arr = [];
        props.spec_1.forEach((sp1) => {
          props.spec_2.forEach((sp2) => {
            barcode_arr.push(
              `99${props.category_code}${sp1.spec_code}${sp2.spec_code}`
            );
          });
        });
        const barcodes = await Barcode.findAll({
          attributes: ["barcode", "price", "discount"],
          where: {
            barcode: barcode_arr,
          },
          order: ["barcode"],
        });
        data = JSON.stringify(barcodes, null, 2);
        break;
      // 單一種類所有品項資料
      case "main":
        const main = await MainView.findAll({
          attributes: [
            "barcode",
            "kind_name",
            "category_name",
            "spec_1_var",
            "spec_2_var",
            "price",
          ],
          where: { category_code: props.category_code },
          order: ["barcode"],
        });
        data = JSON.stringify(main, null, 2);
        break;
      // 所有品項資料
      case "all":
        const all = await MainView.findAll({
          attributes: [
            "barcode",
            "kind_name",
            "category_name",
            "spec_1_var",
            "spec_2_var",
            "price",
          ],
          order: ["barcode"],
        });
        data = JSON.stringify(all, null, 2);
        break;
      // 條碼價格資料
      case "price":
        const price = await Barcode.findAll({
          attributes: ["price"],
          where: { barcode: props.barcode },
        });
        data = JSON.stringify(price, null, 2);
        break;
      case "name_s1":
        const name_s1 = await Spec_name.findAll({
          attributes: ["name"],
          where: { category_code: props.category_code + "s1" },
        });
        data = JSON.stringify(name_s1, null, 2);
        break;
      case "name_s2":
        const name_s2 = await Spec_name.findAll({
          attributes: ["name"],
          where: { category_code: props.category_code + "s2" },
        });
        data = JSON.stringify(name_s2, null, 2);
        break;
      case "discount":
        const discount = await Categories.findAll({
          attributes: ["discount"],
          where: { category_code: props.category_code },
        });
        data = JSON.stringify(discount, null, 2);
        break;
      default:
        return "null";
    }
    return { status: 200, message: data };
  } catch (error) {
    console.log(error.message);
    return { status: 500, message: error.message };
  }
};

// 匯出CSV檔
const ExportCSV = async (target) => {
  const fields = [];
  const kinds = await Kinds.findAll({});
  const categories = await Categories.findAll({});
  const spec = await MainView.findAll({
    attributes: ["kind_name", "category_name", "spec_1_var", "spec_2_var"],
  });
  const main = await MainView.findAll({
    attributes: [
      "barcode",
      "kind_name",
      "category_name",
      "spec_1_var",
      "spec_2_var",
      "price",
    ],
  });

  switch (target) {
    case "kinds":
      fields[0] = "kind_code";
      fields[1] = "kind_name";
      fields[2] = "createdAt";
      fields[3] = "updatedAt";
      const parser_kind = new Parser({ fields });
      const csv_kind = parser_kind.parse(kinds);
      return csv_kind;

    case "categories":
      fields[0] = "category_code";
      fields[1] = "category_name";
      fields[2] = "createdAt";
      fields[3] = "updatedAt";
      const parser_category = new Parser({ fields });
      const csv_category = parser_category.parse(categories);
      return csv_category;

    case "spec":
      fields[0] = "kind_name";
      fields[1] = "category_name";
      fields[2] = "spec_1_var";
      fields[3] = "spec_2_var";
      const parser_spec = new Parser({ fields });
      const csv_spec = parser_spec.parse(spec);
      return csv_spec;

    case "main":
      fields[0] = "barcode";
      fields[1] = "kind_name";
      fields[2] = "category_name";
      fields[3] = "spec_1_var";
      fields[4] = "spec_2_var";
      fields[5] = "price";
      const parser_main = new Parser({ fields });
      const csv_main = parser_main.parse(main);
      return csv_main;

    default:
      return "null";
  }
};

const CreateRoles = () => {
  Roles.create({
    name: "user",
  });
  Roles.create({
    name: "moderator",
  });
  Roles.create({
    name: "admin",
  });
};

module.exports = {
  CreateData_kindgroups: CreateData_kindgroups,
  DestroyData_groups: DestroyData_groups,
  CreateData_kinds: CreateData_kinds,
  DestroyData_kinds: DestroyData_kinds,
  CreateData_categories: CreateData_categories,
  DestroyData_categories: DestroyData_categories,
  CreateData_spec_1: CreateData_spec_1,
  CreateData_spec_2: CreateData_spec_2,
  DestroyData_spec_1: DestroyData_spec_1,
  DestroyData_spec_2: DestroyData_spec_2,
  UpdateData_groups: UpdateData_groups,
  UpdateData_kinds: UpdateData_kinds,
  UpdateData_categories: UpdateData_categories,
  UpdateData_spec_1: UpdateData_spec_1,
  UpdateData_spec_2: UpdateData_spec_2,
  UpdateData_spec_1_name: UpdateData_spec_1_name,
  UpdateData_spec_2_name: UpdateData_spec_2_name,
  UpdateData_barcode: UpdateData_barcode,
  SelectData: SelectData,
  ExportCSV: ExportCSV,
  TruncateData: TruncateData,
  CreateRoles: CreateRoles,
};
