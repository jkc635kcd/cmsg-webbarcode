"use strict";
const { Model, QueryTypes } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class spec_2 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  spec_2.init(
    {
      spec_code: DataTypes.STRING,
      spec_var: DataTypes.STRING,
      category_code: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "spec_2",
      hooks: {
        afterCreate: async function (name, fn) {
          console.log("create barcode from spec_2");
          const queryInterface = sequelize.getQueryInterface();
          const barcode = await queryInterface.sequelize.query(
            'INSERT INTO "barcodes" (barcode) SELECT barcode FROM "barcode_view" WHERE barcode NOT IN (SELECT barcode FROM "barcodes")',
            { type: QueryTypes.INSERT }
          );
        },
        afterBulkDestroy: async () => {
          console.log("spec 2 has destroyed");
        },
      },
    }
  );
  return spec_2;
};
