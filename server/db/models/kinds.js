"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class kinds extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  kinds.init(
    {
      kind_code: DataTypes.STRING,
      kind_name: DataTypes.STRING,
      group_code: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "kinds",
    }
  );
  return kinds;
};
