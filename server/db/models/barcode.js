"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class barcodes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  barcodes.init(
    {
      barcode: DataTypes.STRING,
      price: DataTypes.INTEGER,
      discount: DataTypes.FLOAT,
    },
    {
      sequelize,
      modelName: "barcodes",
    }
  );
  return barcodes;
};
