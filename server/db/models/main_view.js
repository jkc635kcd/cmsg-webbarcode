"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class main_view extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  main_view.init(
    {
      barcode: DataTypes.STRING,
      kind_name: DataTypes.STRING,
      category_name: DataTypes.STRING,
      spec_1_var: DataTypes.STRING,
      spec_2_var: DataTypes.STRING,
      price: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "main_view",
      timestamps: false,
    }
  );
  return main_view;
};
