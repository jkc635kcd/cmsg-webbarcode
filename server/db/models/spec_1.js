"use strict";
const { Model, QueryTypes } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class spec_1 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  spec_1.init(
    {
      spec_code: DataTypes.STRING,
      spec_var: DataTypes.STRING,
      category_code: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "spec_1",
      hooks: {
        afterCreate: async function (name, fn) {
          console.log("create barcode from spec_1");
          const queryInterface = sequelize.getQueryInterface();
          const barcode = await queryInterface.sequelize.query(
            'INSERT INTO "barcodes" (barcode) SELECT barcode FROM "barcode_view" WHERE barcode NOT IN (SELECT barcode FROM "barcodes")',
            { type: QueryTypes.INSERT }
          );
        },
      },
    }
  );
  return spec_1;
};
