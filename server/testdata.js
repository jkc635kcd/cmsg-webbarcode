const db = require("./db");
const fs = require("fs");
const path = require("path");
const csv = require("csv-parser");

const CreateAll_kindgroup = async () => {
  const createGroup = (i) => {
    return new Promise(function (resolve) {
      const res = db.CreateData_kindgroups(`${i}`, `類別群組${i + 1}`);
      resolve(res);
    });
  };
  for (let i = 0; i < 5; i++) {
    const r = await createGroup(i);
    console.log(r);
  }
};

const CreateAll_kinds = async () => {
  const createKind = (code) => {
    return new Promise(function (resolve) {
      const res = db.CreateData_kinds(
        code,
        `類別${code}`,
        code.substring(0, 1)
      );
      resolve(res);
    });
  };
  for (let i = 1; i < 10; i++) {
    let code = i < 10 ? `00${i}` : i < 100 ? `0${i}` : `${i}`;
    console.log(code.substring(1, 3));
    if (code.substring(1, 3) === "00") {
      console.log("none");
    } else {
      const r = await createKind(code);
      console.log(r);
    }
  }
};

const CreateAll_categories = async () => {
  const createCatgory = (kind_code, category_code) => {
    return new Promise(function (resolve) {
      const res = db.CreateData_categories(
        kind_code + category_code,
        `種類${category_code}`,
        `規格規格1`,
        `規格規格2`
      );
      resolve(res);
    });
  };
  let kind_code = 0;
  let category_code = 0;
  for (let i = 1; i < 10; i++) {
    kind_code = i < 10 ? `00${i}` : i < 100 ? `0${i}` : `${i}`;
    if (kind_code.substring(1, 3) === "00") {
      console.log("none");
    } else {
      for (let j = 1; j < 10; j++) {
        category_code = j < 10 ? `00${j}` : j < 100 ? `0${j}` : `${j}`;
        if (category_code.substring(1, 3) === "00") {
          console.log("none");
        } else {
          const r = await createCatgory(kind_code, category_code);
          console.log(r);
        }
      }
    }
  }
};
const CreateAll_spec = async () => {
  const createSpec1 = (spec_code, spec_var, category_code) => {
    return new Promise(function (resolve) {
      const res = db.CreateData_spec_1(spec_code, spec_var, category_code);
      resolve(res);
    });
  };
  const createSpec2 = (spec_code, spec_var, category_code) => {
    return new Promise(function (resolve) {
      const res = db.CreateData_spec_2(spec_code, spec_var, category_code);
      resolve(res);
    });
  };

  let kind_code = 0;
  let category_code = 0;
  for (let i = 1; i < 10; i++) {
    kind_code = i < 10 ? `00${i}` : i < 100 ? `0${i}` : `${i}`;
    if (kind_code.substring(1, 3) === "00") {
      console.log("none");
    } else {
      for (let j = 1; j < 10; j++) {
        category_code = j < 10 ? `00${j}` : j < 100 ? `0${j}` : `${j}`;
        if (category_code.substring(1, 3) === "00") {
          console.log("none");
        } else {
          for (let k = 1; k < 50; k++) {
            let spec = k < 10 ? `0${k}` : `${k}`;
            const r1 = await createSpec1(
              spec,
              `參數${k}`,
              kind_code + category_code
            );
            const r2 = await createSpec2(
              spec,
              `參數${k}`,
              kind_code + category_code
            );
            console.log(r1);
            console.log(r2);
          }
        }
      }
    }
  }
};

const UpdatePrice = async () => {
  for (let i = 1; i < 11; i++) {
    for (let j = 1; j < 11; j++) {
      let a = i;
      let b = j;
      a >= 10 ? (a = String(i)) : (a = "0" + i);
      b >= 10 ? (b = String(j)) : (b = "0" + j);
      db.UpdateData_barcode(`99001001${a}${b}`, Math.floor(Math.random() * 99));
    }
  }
};

const CreateAll = async () => {
  const delay = (s) => {
    return new Promise(function (resolve) {
      setTimeout(resolve, s);
    });
  };
  await CreateAll_kindgroup();
  await delay(1000);
  await CreateAll_kinds();
  await delay(1000);
  await CreateAll_categories();
  await delay(1000);
  await CreateAll_spec();
  await delay(1000);
  db.CreateRoles();
};

const ReadFile = () => {
  try {
    fs.createReadStream(
      "/home/user/Documents/staff_share/BarcodeDB/csv/001_螺絲(零售)/001_六角螺絲/種類一_規格一.csv"
    )
      .pipe(csv())
      .on("data", (data) => results.push(data))
      .on("end", () => {
        console.log(results);
        // res.send(p.parse(results));
        // res.send(results);
      });
  } catch (error) {
    console.error(error.message);
  }
};

// 新增類別
const CreateKind = async (kind_code, kind_name) => {
  await db.CreateData_kinds(kind_code, kind_name);
};

const DestroyKind = async (kind_code) => {
  await db.DestroyData_kinds(kind_code);
};

// 新增種類
const CreateCategory = async (category_code, category_name, kind_code) => {
  await db.CreateData_categories(category_code, category_name, kind_code);
};

// 新增規格
const CreateSpec = async (spec_code, spec_var_1, spec_var_2, category_code) => {
  await db.CreateData_spec_1(spec_code, spec_var_1, category_code);
  await db.CreateData_spec_2(spec_code, spec_var_2, category_code);
};

// 取得資料
const SelectData = async (target) => {
  const data = await db.SelectData(target);
  console.log(data);
};
// 匯出CSV
const ExportCSV = async (target) => {
  const data = await db.ExportCSV(target);
  console.log(data);
};

// ExportCSV("spec");

CreateAll();

// UpdatePrice();

// ReadFile();
// controller.InitialDB();
