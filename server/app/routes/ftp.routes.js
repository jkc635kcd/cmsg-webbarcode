var multer = require("multer");
const { authJwt } = require("../middleware");
const { uploadFile, deleteFile } = require("../controllers/ftp.contorllers");

// 上傳圖片multer設定
const upload = (type) => {
  switch (type) {
    case "image":
      return multer({
        limits: {
          fileSize: 1000000, // 最大不超過1MB
        },
        fileFilter(req, file, cb) {
          if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
            cb(new Error("Please upload an image"));
          }
          cb(null, true);
        },
      });
    case "file":
      return multer({
        limits: {
          fileSize: 10000000, // 最大不超過10MB
        },
        fileFilter(req, file, cb) {
          if (!file.originalname.match(/\.(pdf)$/)) {
            cb(new Error("Please upload an pdf"));
          }
          cb(null, true);
        },
      });
  }
};

module.exports = (app) => {
  app.use((req, res, next) => {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  // 上傳商品圖片(條碼,種類或單一規格,檔案權限)
  app.post(
    "/v1/shop/image",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    upload("image").single("image"),
    async (req, res) => {
      const { code, type, permissions } = req.query;
      const target = {
        code: code,
        type: type,
        source: req.file,
        permissions: permissions,
      };
      try {
        console.log("test");
        await uploadFile(target);
        res.send("success");
      } catch (error) {
        console.log(error);
        res.status(500).send(error);
      }
    }
  );

  // 上傳商品文件(條碼,種類或單一規格,檔案權限)
  app.post(
    "/shop/file",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    upload("file").single("file"),
    async (req, res) => {
      const { code, type, permissions } = req.query;
      const target = {
        code: code,
        type: type,
        source: req.file,
        permissions: permissions,
      };

      try {
        await uploadFile(target);
        res.send("success");
      } catch (error) {
        console.log(error);
        res.status(500).send(error);
      }
    }
  );

  // 刪除圖片(編號,種類或單一規格)
  app.delete(
    "/v1/shop/image",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      const { code, type } = req.query;
      const target = {
        code: code,
        type: type,
        fileType: "image",
      };
      try {
        await deleteFile(target);
        res.send("success");
      } catch (error) {
        console.log(error);
        res.status(500).send(error);
      }
    }
  );

  // 刪除檔案(編號,種類或單一規格)
  app.delete(
    "/v1/shop/file",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      const { code, type } = req.query;
      const target = {
        code: code,
        type: type,
        fileType: "file",
      };
      try {
        await deleteFile(target);
        res.send("success");
      } catch (error) {
        console.log(error);
        res.status(500).send(error);
      }
    }
  );
};
