const db = require("../../db");
const { authJwt } = require("../middleware");

// 判斷code代號正確性
const checkCode = (type, code) => {
  if (type == "group") {
    if (/^[0-9]$/.test(code)) {
      return true;
    } else {
      return false;
    }
  } else if (type == "kind") {
    if (/^[0-9][0-9][0-9]$/.test(code) && code != 000) {
      return true;
    } else {
      return false;
    }
  } else if (type == "category") {
    if (
      /^[0-9][0-9][0-9][0-9][0-9][0-9]$/.test(code) &&
      code.substring(0, 3) != 000 &&
      code.substring(3, 6) != 000
    ) {
      return true;
    } else {
      return false;
    }
  } else if (type == "spec") {
    if (/^[0-9][0-9]$/.test(code) && code != 00) {
      return true;
    } else {
      return false;
    }
  }
};

module.exports = (app) => {
  app.use((req, res, next) => {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  // 取得類別群組資料
  app.get("/v1/groups", async (req, res) => {
    try {
      const target = { target: "groups" };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  // 新增類別群組
  app.post(
    "/v1/groups",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { group_code, group_name } = req.query;
        if (!checkCode("group", group_code)) {
          res.status(400).send("group_code must be 0~9");
          return;
        }
        const response = await db.CreateData_kindgroups(group_code, group_name);
        if (response.status === 510) {
          res.status(510).send(response.message);
        } else {
          res.send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 更新類別群組
  app.patch(
    "/v1/groups",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { group_code, group_name } = req.query;
        if (!checkCode("group", group_code)) {
          res.status(400).send("群組編號必須介於 0~9");
          return;
        }
        if (group_name == "") {
          res.status(400).send("群組名稱不可爲空");
          return;
        }
        const target = {
          group_code: group_code,
          group_name: group_name,
        };
        const response = await db.UpdateData_groups(target);
        console.log(response);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 刪除類別群組
  app.delete(
    "/v1/groups",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { group_code } = req.query;
        if (!checkCode("group", group_code)) {
          res.status(400).send("group_code must be 0~9");
          return;
        }
        const response = await db.DestroyData_groups(group_code);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 取得類別資料
  app.get("/v1/kinds", async (req, res) => {
    try {
      const { group_code } = req.query;
      if (!checkCode("group", group_code)) {
        res.status(400).send("group_code must be 0~9");
        return;
      }
      const target = { target: "kinds", group_code: group_code };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  // 新增類別
  app.post(
    "/v1/kinds",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { kind_code, kind_name } = req.query;
        const group_code = kind_code.substring(0, 1);
        if (!checkCode("group", group_code)) {
          res.status(400).send("group_code must be 0~9");
          return;
        }
        if (!checkCode("kind", kind_code)) {
          res.status(400).send("kind_code must be 001~999");
          return;
        }
        const response = await db.CreateData_kinds(
          kind_code,
          kind_name,
          group_code
        );
        if (response.status === 510) {
          res.status(510).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 更新類別資料
  app.patch(
    "/v1/kinds",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { kind_code, kind_name } = req.query;
        if (!checkCode("kind", kind_code)) {
          res.status(400).send("kind_code must be 001~999");
          return;
        }
        const target = {
          kind_code: kind_code,
          kind_name: kind_name,
        };
        const response = await db.UpdateData_kinds(target);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 刪除類別
  app.delete(
    "/v1/kinds",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { kind_code } = req.query;
        if (!checkCode("kind", kind_code)) {
          res.status(400).send("kind_code must be 001~999");
          return;
        }
        const response = await db.DestroyData_kinds(kind_code);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {}
    }
  );

  // 取得所有種類資料
  app.get("/v1/categories", async (req, res) => {
    try {
      const target = { target: "categories" };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  // 取得指定種類資料
  app.get("/v1/category", async (req, res) => {
    try {
      const { kind_code } = req.query;
      if (!checkCode("kind", kind_code)) {
        res.status(400).send("kind_code must be 001~999");
        return;
      }
      const target = { target: "category", kind_code: kind_code };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  // 新增種類
  app.post(
    "/v1/categories",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { category_code, category_name, spec_1_name, spec_2_name } =
          req.query;
        if (!checkCode("category", category_code)) {
          res.status(400).send("category_code must be 001001~999999");
          return;
        }
        const response = await db.CreateData_categories(
          category_code,
          category_name,
          spec_1_name,
          spec_2_name
        );
        if (response.status === 510) {
          res.status(510).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 更新種類資料
  app.patch(
    "/v1/categories",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { category_code, category_name, discount } = req.query;
        if (!checkCode("category", category_code)) {
          res.status(400).send("category_code must be 001001~999999");
          return;
        }
        const target = {
          category_code: category_code,
          category_name: category_name,
          discount: discount,
        };
        const response = await db.UpdateData_categories(target);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 刪除種類
  app.delete(
    "/v1/categories",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { category_code } = req.query;
        if (!checkCode("category", category_code)) {
          res.status(400).send("category_code must be 001001~999999");
          return;
        }
        const response = await db.DestroyData_categories(category_code);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 取得所有規格資料
  app.get("/v1/spec/value/all", async (req, res) => {
    try {
      const { category_code } = req.query;
      if (!checkCode("category", category_code)) {
        res.status(400).send("category_code must be 001001~999999");
        return;
      }
      const target = { target: "spec", category_code: category_code };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  // 取得規格(一)資料
  app.get("/v1/spec/value/1", async (req, res) => {
    try {
      const { category_code, page, pagination } = req.query;
      if (!checkCode("category", category_code)) {
        res.status(400).send("category_code must be 001001~999999");
        return;
      }
      const target = {
        target: "spec_1",
        category_code: category_code,
        page: page,
        pagination: pagination,
      };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  // 取得規格(二)資料
  app.get("/v1/spec/value/2", async (req, res) => {
    try {
      const { category_code } = req.query;
      if (!checkCode("category", category_code)) {
        res.status(400).send("category_code must be 001001~999999");
        return;
      }
      const target = {
        target: "spec_2",
        category_code: category_code,
      };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  // 新增規格(一)
  app.post(
    "/v1/spec/value/1",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { category_code, spec_code, spec_var_1 } = req.query;
        const response = await db.CreateData_spec_1(
          spec_code,
          spec_var_1,
          category_code
        );
        if (!checkCode("category", category_code)) {
          res.status(400).send("category_code must be 001001~999999");
          return;
        }
        if (!checkCode("spec", spec_code)) {
          res.status(400).send("spec_code must be 01~99");
          return;
        }
        if (response.status === 510) {
          res.status(510).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 新增規格(二)
  app.post(
    "/v1/spec/value/2",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { category_code, spec_code, spec_var_2 } = req.query;
        const response = await db.CreateData_spec_2(
          spec_code,
          spec_var_2,
          category_code
        );
        if (!checkCode("category", category_code)) {
          res.status(400).send("category_code must be 001001~999999");
          return;
        }
        if (!checkCode("spec", spec_code)) {
          res.status(400).send("spec_code must be 01~99");
          return;
        }
        if (response.status === 510) {
          res.status(510).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 更新規格(一)
  app.patch(
    "/v1/spec/value/1",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { category_code, spec_code, spec_var } = req.query;
        if (!checkCode("spec", spec_code)) {
          res.status(400).send("spec_code must be 01~99");
          return;
        }
        const target = {
          category_code: category_code,
          spec_code: spec_code,
          spec_var: spec_var,
        };
        const response = await db.UpdateData_spec_1(target);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 更新規格(二)
  app.patch(
    "/v1/spec/value/2",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { category_code, spec_code, spec_var } = req.query;
        if (!checkCode("spec", spec_code)) {
          res.status(400).send("spec_code must be 01~99");
          return;
        }
        const target = {
          category_code: category_code,
          spec_code: spec_code,
          spec_var: spec_var,
        };
        const response = await db.UpdateData_spec_2(target);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 更新規格名稱(一)
  app.patch(
    "/v1/spec/name/1",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { category_code, name } = req.query;
        if (!checkCode("category", category_code)) {
          res.status(400).send("category_code must be 001001~999999");
          return;
        }
        const target = {
          category_code: category_code,
          name: name,
        };
        const response = await db.UpdateData_spec_1_name(target);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 更新規格名稱(二)
  app.patch(
    "/v1/spec/name/2",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { category_code, name } = req.query;
        if (!checkCode("category", category_code)) {
          res.status(400).send("category_code must be 001001~999999");
          return;
        }
        const target = {
          category_code: category_code,
          name: name,
        };
        const response = await db.UpdateData_spec_2_name(target);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 刪除規格(一)
  app.delete(
    "/v1/spec/value/1",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { category_code, spec_code } = req.query;
        if (!checkCode("category", category_code)) {
          res.status(400).send("category_code must be 001001~999999");
          return;
        }
        if (!checkCode("spec", spec_code)) {
          res.status(400).send("spec_code must be 01~99");
          return;
        }
        const response = await db.DestroyData_spec_1(category_code, spec_code);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 刪除規格(二)
  app.delete(
    "/v1/spec/value/2",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { category_code, spec_code } = req.query;
        if (!checkCode("category", category_code)) {
          res.status(400).send("category_code must be 001001~999999");
          return;
        }
        if (!checkCode("spec", spec_code)) {
          res.status(400).send("spec_code must be 01~99");
          return;
        }
        const response = await db.DestroyData_spec_2(category_code, spec_code);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 取得種類條碼資料
  app.get("/v1/barcodes/all", async (req, res) => {
    try {
      const { category_code } = req.query;
      if (!checkCode("category", category_code)) {
        res.status(400).send("category_code must be 001001~999999");
        return;
      }
      const target = {
        target: "barcodes_all",
        category_code: category_code,
      };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  // 取得條碼分頁資料
  app.get("/v1/barcodes/page", async (req, res) => {
    try {
      const { category_code, page, pagination } = req.query;
      if (!checkCode("category", category_code)) {
        res.status(400).send("category_code must be 001001~999999");
        return;
      }

      const spec_1 = await db.SelectData({
        target: "spec_1",
        category_code: category_code,
        page: page,
        pagination: pagination,
      });
      const spec_2 = await db.SelectData({
        target: "spec_2",
        category_code: category_code,
      });
      const target = {
        target: "barcodes",
        category_code: category_code,
        spec_1: JSON.parse(spec_1.message).rows,
        spec_2: JSON.parse(spec_2.message),
      };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  // 取得價錢資料
  app.get("/v1/price", async (req, res) => {
    try {
      const { barcode } = req.query;
      const target = { target: "price", barcode: barcode };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  // 取得所有資料(含條碼/價格)
  app.get("/v1/all", async (req, res) => {
    try {
      const data = await db.SelectData("all");
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  app.get("/v1/main", async (req, res) => {
    try {
      const { category_code } = req.query;
      if (!checkCode("category", category_code)) {
        res.status(400).send("category_code must be 001001~999999");
        return;
      }
      const target = { target: "main", category_code: category_code };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  app.get("/v1/spec/name/1", async (req, res) => {
    try {
      const { category_code } = req.query;
      if (!checkCode("category", category_code)) {
        res.status(400).send("category_code must be 001001~999999");
        return;
      }
      const target = { target: "name_s1", category_code: category_code };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  app.get("/v1/spec/name/2", async (req, res) => {
    try {
      const { category_code } = req.query;
      if (!checkCode("category", category_code)) {
        res.status(400).send("category_code must be 001001~999999");
        return;
      }
      const target = { target: "name_s2", category_code: category_code };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  // 取得種類折數
  app.get("/v1/discount", async (req, res) => {
    try {
      const { category_code } = req.query;
      if (!checkCode("category", category_code)) {
        res.status(400).send("category_code must be 001001~999999");
        return;
      }
      const target = { target: "discount", category_code: category_code };
      const data = await db.SelectData(target);
      if (data.status === 500) {
        res.status(500).send(data.message);
      } else {
        res.status(200).send(JSON.parse(data.message));
      }
    } catch (error) {
      console.error(error.message);
      res.status(400).send(error.message);
    }
  });

  // 更新種類折數
  app.patch(
    "/v1/discount",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { category_code, discount } = req.query;
        if (!checkCode("category", category_code)) {
          res.status(400).send("category_code must be 001001~999999");
          return;
        }
        const response = await db.UpdateData_categories(
          category_code,
          discount
        );
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );

  // 更新價錢
  app.patch(
    "/v1/price",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    async (req, res) => {
      try {
        const { barcode, price, discount } = req.query;
        const response = await db.UpdateData_barcode(barcode, price, discount);
        if (response.status === 500) {
          res.status(500).send(response.message);
        } else {
          res.status(200).send(response.message);
        }
      } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
      }
    }
  );
};
