const { authJwt } = require("../middleware");
const controller = require("../controllers/user.controllers");

module.exports = (app) => {
  app.use((req, res, next) => {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/v1/verify/all", controller.allAccess);

  app.get("/v1/verify/user", [authJwt.verifyToken], controller.userAccess);

  app.get(
    "/v1/verify/moderator",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.moderatorAccess
  );

  app.get(
    "/v1/verify/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminAccess
  );
};
