const { verifySignUp } = require("../middleware");
const { authJwt } = require("../middleware");
const controller = require("../controllers/auth.controllers");
module.exports = (app) => {
  app.use((req, res, next) => {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    res.header("Access-Control-Allow-Headers", "Set-Cookie");
    next();
  });

  // get csrf token
  app.get("/v1/auth/csrf", async (req, res) => {
    res.json({ csrfToken: req.csrfToken() });
  });

  app.post(
    "/v1/auth/signup",
    [
      verifySignUp.checkDuplicateUsernameOrEmail,
      verifySignUp.checkRolesExisted,
    ],
    controller.signup
  );

  app.post("/v1/auth/login", controller.signin);
  app.post("/v1/auth/setcookie", [authJwt.verifyToken], controller.setcookie);
  app.post("/v1/auth/logout", [authJwt.verifyToken], controller.logout);
  app.get("/v1/auth/profile", [authJwt.verifyToken], controller.getUserProfile);
};
