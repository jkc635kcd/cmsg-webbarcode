require("dotenv").config();
const ftp = require("basic-ftp");
const { Readable } = require("stream");
const user = {
  host: process.env.FTP_HOST,
  user: process.env.FTP_USER,
  password: process.env.FTP_PASSWORD,
  sercure: process.env.FTP_SERCURE,
};

// 上傳檔案(目標參數物件)
async function uploadFile(target) {
  const client = new ftp.Client();

  // 顯示傳輸檔案過程
  client.ftp.verbose = true;
  // 變更檔案權限
  const changePermissions = (perms, filepath) => {
    let cmd = `SITE CHMOD ${perms} ${filepath}`;
    return client.send(cmd, false);
  };

  try {
    // 以條碼編號命名檔案
    const fileName =
      target.type == "category"
        ? `${target.code.substring(2, 8)}`
        : `${target.code}`;

    // 目標根目錄/群組(編號)/類別(編號)/種類(編號)
    let remotePath = `${process.env.FILE_ROOT}/${target.code.substring(
      2,
      3
    )}/${target.code.substring(2, 5)}/${target.code.substring(2, 8)}`;

    // 取得副檔名
    const origin = target.source.originalname;
    let ext = origin.substring(origin.split("").lastIndexOf(".") + 1);

    // 資料夾分類
    if (origin.match(/\.(jpg|jpeg|png)$/)) {
      remotePath = `${remotePath}/image`;
      ext = "jpg";
    } else if (origin.match(/\.(pdf)$/)) {
      remotePath = `${remotePath}/pdf`;
    }

    // 將buffer存進stream資料裡
    const imgStream = new Readable();
    imgStream._read = () => {};
    imgStream.push(target.source.buffer);
    imgStream.push(null);

    // 存取目標FTP
    await client.access(user);

    // 產生並切換至指定遠端資料夾
    await client.ensureDir(remotePath);

    // 上傳buffer資料
    await client.uploadFrom(imgStream, `${fileName}.${ext}`);

    // 變更檔案權限
    await changePermissions(
      target.permissions.toString(),
      `/${remotePath}/${fileName}.${ext}`
    );
  } catch (error) {
    console.log(error);
    throw error;
  }
  client.close();
}

// 刪除指定檔案
async function deleteFile(target) {
  const client = new ftp.Client();
  client.ftp.verbose = true;
  try {
    // 檔案名稱
    const fileName =
      target.type == "category"
        ? `${target.code.substring(2, 8)}`
        : `${target.code}`;

    // 目標根目錄/群組(編號)/類別(編號)/種類(編號)
    let remotePath = `${process.env.FILE_ROOT}/${target.code.substring(
      2,
      3
    )}/${target.code.substring(2, 5)}/${target.code.substring(2, 8)}`;

    switch (target.fileType) {
      case "image":
        remotePath = `${remotePath}/image`;
        break;
      case "file":
        remotePath = `${remotePath}/pdf`;
        break;
    }
    await client.access(user);
    let path = await client.list(remotePath);
    for (let i = 0; i < path.length; i++) {
      const name = path[i].name;
      console.log(name);
      if (path[i].name.split(".")[0] == target.code) {
        remotePath = `${remotePath}/${name}`;
        break;
      }
    }

    await client.remove(remotePath);
  } catch (error) {
    console.log(error);
    throw error;
  }
  client.close;
}

module.exports = {
  uploadFile: uploadFile,
  deleteFile: deleteFile,
};
