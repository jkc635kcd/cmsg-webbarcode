const db = require("../../db/models");
const jwt = require("jsonwebtoken");
const config = require("../config/auth.config");
const User = db.user;
const Role = db.role;

const Op = db.Sequelize.Op;

exports.allAccess = (req, res) => {
  res.status(200).send("Access as Public");
};

exports.userAccess = (req, res) => {
  res.status(200).send("Access as User");
};

exports.moderatorAccess = (req, res) => {
  res.status(200).send("Access as Moderator");
};

exports.adminAccess = (req, res) => {
  res.status(200).send("Access as Admin");
};
