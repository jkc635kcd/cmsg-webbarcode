const db = require("../../db/models");
const config = require("../config/auth.config");
const User = db.user;
const Role = db.role;

const Op = db.Sequelize.Op;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
  // Save User to Database
  try {
    User.create({
      username: req.body.username,
      password: bcrypt.hashSync(req.body.password, 10),
    }).then((user) => {
      if (req.body.roles) {
        Role.findAll({
          where: {
            name: { [Op.or]: req.body.roles },
          },
        }).then((roles) => {
          user.setRoles(roles).then(() => {
            res.send({ message: "User was registered successfully!" });
          });
        });
      } else {
        user.setRoles([1]).then(() => {
          res.send({ message: "User was registered successfully!" });
        });
      }
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: error.message });
  }
};

exports.signin = (req, res) => {
  try {
    User.findOne({
      where: {
        username: req.body.username,
      },
    }).then((user) => {
      if (!user) {
        return res.status(404).send({ message: "帳號未註冊!" });
      }

      var passwordIsVaild = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsVaild) {
        return res.status(401).send({
          accessToken: null,
          message: "密碼錯誤!",
        });
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400, // 24 hours
      });

      var authorities = [];
      user.getRoles().then(async (roles) => {
        for (let i = 0; i < roles.length; i++) {
          authorities.push("ROLE_" + roles[i].name.toUpperCase());
        }

        res.cookie("authcookie", token, {
          maxAge: 86400 * 1000,
          httpOnly: true,
          samSite: "lax",
        });
        let role = "";
        if (roles.length > 2) {
          role = "系統管理員";
        } else if (roles.length > 1) {
          role = "系統維護員";
        } else {
          role = "一般使用者";
        }
        res.status(200).send("登入成功,身份: " + role);
      });
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: error.message });
  }
};

exports.setcookie = (req, res) => {
  User.update(
    {
      token: req.cookies.authcookie,
      updatedAt: new Date(),
    },
    {
      where: { username: req.body.username },
    }
  );
};

exports.logout = (req, res) => {
  User.update(
    {
      token: "",
      updatedAt: new Date(),
    },
    {
      where: { token: req.cookies.authcookie },
    }
  );
  res.cookie("authcookie", "", {
    expires: new Date(Date.now() + 3 * 1000),
    httpOnly: true,
  });
  res.status(200).json({ success: true, message: "已登出系統!" });
};

exports.getUserProfile = (req, res) => {
  let authcookie = req.cookies.authcookie;
  jwt.verify(authcookie, config.secret, (err, decoded) => {
    if (err) {
      res.status(401).send({
        message: "權限不符!",
      });
    }
    req.userId = decoded.id;
  });

  User.findByPk(req.userId).then((user) => {
    user.getRoles().then((roles) => {
      let userData = {};
      if (roles.length > 2) {
        userData.role = "系統管理員";
      } else if (roles.length > 1) {
        userData.role = "系統維護員";
      } else {
        userData.role = "一般使用者";
      }
      userData.name = user.dataValues.username;
      userData.login = user.dataValues.updatedAt;
      res.status(200).send(userData);
    });
  });
};
