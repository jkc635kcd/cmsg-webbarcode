const jwt = require("jsonwebtoken");
const config = require("../config/auth.config");
const db = require("../../db/models");
const User = db.user;

verifyToken = (req, res, next) => {
  const authcookie = req.cookies.authcookie;

  if (!authcookie) {
    return res.status(403).send({
      message: "未登入系統!",
    });
  }

  jwt.verify(authcookie, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "權限不符!",
      });
    }
    req.userId = decoded.id;
    next();
  });
};

isAdmin = (req, res, next) => {
  User.findByPk(req.userId).then((user) => {
    user.getRoles().then((roles) => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === "admin") {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "僅 <系統管理員> 身份才可執行!",
      });
      return;
    });
  });
};

isModerator = (req, res, next) => {
  User.findByPk(req.userId).then((user) => {
    user.getRoles().then((roles) => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === "moderator") {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "僅 <系統維護員> 身份才可執行!",
      });
    });
  });
};

isModeratorOrAdmin = (req, res, next) => {
  User.findByPk(req.userId).then((user) => {
    user.getRoles().then((roles) => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === "moderator") {
          next();
          return;
        }

        if (roles[i].name === "admin") {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "僅 <系統管理員或系統維護員> 身份才可執行!",
      });
    });
  });
};

const authJwt = {
  verifyToken: verifyToken,
  isAdmin: isAdmin,
  isModerator: isModerator,
  isModeratorOrAdmin: isModeratorOrAdmin,
};
module.exports = authJwt;
