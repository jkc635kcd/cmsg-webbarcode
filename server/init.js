require("dotenv").config();
const db = require("./db/models");
const Role = db.role;

function initial() {
  Role.create({
    id: 1,
    name: "user",
  });
  Role.create({
    id: 2,
    name: "moderator",
  });
  Role.create({
    id: 3,
    name: "admin",
  });
}

// db.sequelize.sync();
db.sequelize.sync({ force: false }).then(() => {
  console.log("Drop and Resync Db");
  initial();
});
