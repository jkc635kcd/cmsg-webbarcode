import { Notification } from "../components/Notification";
import UserService from "../services/user.service";
import axios from "axios";
const host = window.location.hostname;
const protocol = window.location.protocol;
const version = process.env.REACT_APP_API_VERSION;
const api = `${protocol}//${host}/api${version}`;

const handleAuth = (res) => {
  console.log(res);
};

const handleGetData = (res) => {
  let data = res.json();
  switch (res.status) {
    case 200:
      return data;
    case 400:
      Notification("error", "代號格式錯誤", `訊息: ${data}`);
      break;
    case 500:
      Notification("error", "資料庫錯誤", `訊息: ${data}`);
      break;
    default:
      Notification("error", "未知錯誤", `訊息: ${data}`);
      break;
  }
};

const handleEditData = async (res) => {
  let data = await res.text();
  switch (res.status) {
    case 200:
      return data;
    case 400:
      Notification("error", "代號格式錯誤", `訊息: ${data}`);
      break;
    case 500:
      Notification("error", "資料庫錯誤", `訊息: ${data}`);
      break;
    case 510:
      Notification("error", "新增資料失敗", `訊息: 編號已使用  (${data})`);
      break;
    default:
      Notification("error", "未知錯誤", `訊息: ${data}`);
      break;
  }
  return false;
};

export const GetKindgroups = async () => {
  try {
    const res = await fetch(`${api}/groups`);
    const jsonData = await handleGetData(res);
    return jsonData;
  } catch (error) {
    console.log(error);
    Notification("error", "取得類別群組失敗", `訊息: ${error}`);
  }
};

export const GetKinds = async (group_code) => {
  try {
    const res = await fetch(`${api}/kinds?group_code=${group_code}`);
    const jsonData = await handleGetData(res);
    return jsonData;
  } catch (error) {
    console.log(error);
    Notification("error", "取得類別失敗", `訊息: ${error}`);
  }
};

export const GetCategories = async (kind_code) => {
  try {
    const res = await fetch(`${api}/category?kind_code=${kind_code}`);
    const jsonData = await handleGetData(res);
    return jsonData;
  } catch (error) {
    console.log(error);
    Notification("error", "取得種類失敗", `訊息: ${error}`);
  }
};

export const GetSpec_1 = async (category_code, page, pagination) => {
  try {
    const res = await fetch(
      `${api}/spec/value/1?category_code=${category_code}&page=${page}&pagination=${pagination}`
    );
    const jsonData = await handleGetData(res);
    return jsonData;
  } catch (error) {
    console.log(error);
    Notification("error", "取得規格一失敗", `訊息: ${error}`);
  }
};

export const GetSpec_2 = async (category_code) => {
  try {
    const res = await fetch(
      `${api}/spec/value/2?category_code=${category_code}`
    );
    const jsonData = await handleGetData(res);
    return jsonData;
  } catch (error) {
    console.log(error);
    Notification("error", "取得規格二失敗", `訊息: ${error}`);
  }
};

export const GetSpec_1_name = async (category_code) => {
  try {
    const res = await fetch(
      `${api}/spec/name/1?category_code=${category_code}`
    );
    const jsonData = await handleGetData(res);
    return jsonData;
  } catch (error) {
    console.log(error);
  }
};

export const GetSpec_2_name = async (category_code) => {
  try {
    const res = await fetch(
      `${api}/spec/name/2?category_code=${category_code}`
    );
    const jsonData = await handleGetData(res);
    return jsonData;
  } catch (error) {
    console.log(error);
  }
};

export const GetAllBarcodes = async (category_code) => {
  try {
    let result = {};
    const res = await fetch(
      `${api}/barcodes/all?category_code=${category_code}`
    );
    const jsonData = await handleGetData(res);
    jsonData.forEach((element, index) => {
      let barcode = element.barcode;
      let price = element.price;
      let discount = element.discount;
      result[barcode] = price;
      result[`${barcode}_discount`] = discount;
    });
    return result;
  } catch (error) {
    console.log(error);
    Notification("error", "取得條碼失敗", `訊息: ${error}`);
  }
};

export const GetBarcodes = async (category_code, page, pagination) => {
  try {
    let result = {};
    const res = await fetch(
      `${api}/barcodes/page?category_code=${category_code}&page=${page}&pagination=${pagination}`
    );
    const jsonData = await handleGetData(res);
    jsonData.forEach((element, index) => {
      let barcode = element.barcode;
      let price = element.price;
      let discount = element.discount;
      result[barcode] = price;
      result[`${barcode}_discount`] = discount;
    });
    return result;
  } catch (error) {
    console.log(error);
    Notification("error", "取得條碼失敗", `訊息: ${error}`);
  }
};

export const CreateKindGroup = async (group_code, group_name) => {
  try {
    const res = await fetch(
      `${api}/groups?group_code=${group_code}&group_name=${group_name}`,
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification(
        "success",
        "新增類別群組",
        `編號:${group_code} 值:${group_name}`
      );
    }
    return res;
  } catch (error) {
    console.log(error);
    Notification("error", "新增類別群組失敗", `訊息: ${error}`);
  }
};

export const CreateKind = async (kind_code, kind_name, group_code) => {
  try {
    const res = await fetch(
      `${api}/kinds?kind_code=${kind_code}&kind_name=${kind_name}&group_code=${group_code}`,
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    // const res = await axios(
    //   `${http}://${host}${api}/kinds?kind_code=${kind_code}&kind_name=${kind_name}&group_code=${group_code}`,
    //   {
    //     method: "POST",
    //     headers: { "Content-Type": "application/json" },
    //     withCredentials: true,
    //   }
    // );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification("success", "新增類別", `編號: ${kind_code} 值:${kind_name}`);
    }
    return res;
  } catch (error) {
    console.log(error);
    Notification("error", "新增類別失敗", `訊息: ${error}`);
  }
};

export const CreateCategory = async (
  category_code,
  category_name,
  spec_1_name,
  spec_2_name
) => {
  try {
    const res = await fetch(
      `${api}/categories?category_code=${category_code}&category_name=${category_name}&spec_1_name=${spec_1_name}&spec_2_name=${spec_2_name}`,
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification(
        "success",
        "新增種類",
        `編號: ${category_code} 值:${category_name} 規格(欄):${spec_1_name} 規格(列):${spec_2_name}`
      );
    }
    return res;
  } catch (error) {
    console.log(error);
    Notification("error", "新增種類失敗", `訊息: ${error}`);
  }
};

export const CreateSpec_1 = async (spec_code, spec_var, category_code) => {
  try {
    const res = await fetch(
      `${api}/spec/value/1?category_code=${category_code}&spec_code=${spec_code}&spec_var_1=${spec_var}`,
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification("success", "新增規格一", `編號:${spec_code} 值:${spec_var}`);
    }
    return res;
  } catch (error) {
    console.log(error);
    Notification("error", "新增規格一失敗", `訊息: ${error}`);
  }
};

export const CreateSpec_2 = async (spec_code, spec_var, category_code) => {
  try {
    const res = await fetch(
      `${api}/spec/value/2?category_code=${category_code}&spec_code=${spec_code}&spec_var_2=${spec_var}`,
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification("success", "新增規格二", `編號:${spec_code} 值:${spec_var}`);
    }
    return res;
  } catch (error) {
    console.log(error);
    Notification("error", "新增規格二失敗", `訊息: ${error}`);
  }
};

export const UpdateKindGroup = async (group_code, group_name) => {
  try {
    const res = await fetch(
      `${api}/groups?group_code=${group_code}&group_name=${group_name}`,
      {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification(
        "success",
        "更新類別群組",
        `編號:${group_code},值:${group_name}`
      );
    }
  } catch (error) {
    console.log(error);
    Notification("error", "更新類別群組失敗", `訊息: ${error}`);
  }
};

export const UpdateKind = async (kind_code, kind_name) => {
  try {
    const res = await fetch(
      `${api}/kinds?kind_code=${kind_code}&kind_name=${kind_name}`,
      {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification("success", "更新類別", `編號:${kind_code},值:${kind_name}`);
    }
  } catch (error) {
    console.log(error);
    Notification("error", "更新類別群組失敗", `訊息: ${error}`);
  }
};

export const UpdateCategory = async (
  category_code,
  category_name,
  discount
) => {
  try {
    const res = await fetch(
      `${api}/categories?category_code=${category_code}&category_name=${category_name}&discount=${discount}`,
      {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification(
        "success",
        "更新種類",
        `編號:${category_code},值:${category_name}, 折數:${discount}`
      );
    }
  } catch (error) {
    console.log(error);
    Notification("error", "更新種類失敗", `訊息: ${error}`);
  }
};

export const UpdateSpec_1 = async (category_code, spec_code, spec_var) => {
  try {
    const res = await fetch(
      `${api}/spec/value/1?category_code=${category_code}&spec_code=${spec_code}&spec_var=${spec_var}`,
      {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification(
        "success",
        "更新規格(一)",
        `編號:${spec_code},值:${spec_var}`
      );
    }
  } catch (error) {
    console.log(error);
    Notification("error", "更新規格(一)失敗", `訊息: ${error}`);
  }
};

export const UpdateSpec_2 = async (category_code, spec_code, spec_var) => {
  try {
    const res = await fetch(
      `${api}/spec/value/2?category_code=${category_code}&spec_code=${spec_code}&spec_var=${spec_var}`,
      {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification(
        "success",
        "更新規格(二)",
        `編號:${spec_code},值:${spec_var}`
      );
    }
  } catch (error) {
    console.log(error);
    Notification("error", "更新規格(二)失敗", `訊息: ${error}`);
  }
};

export const UpdateSpec_1_name = async (category_code, name) => {
  try {
    const res = await fetch(
      `${api}/spec/name/1?category_code=${category_code}&name=${name}`,
      {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification(
        "success",
        "更新規格(一)名稱",
        `編號:${category_code},值:${name}`
      );
    }
  } catch (error) {
    console.log(error);
    Notification("error", "更新規格(一)名稱失敗", `訊息: ${error}`);
  }
};

export const UpdateSpec_2_name = async (category_code, name) => {
  try {
    const res = await fetch(
      `${api}/spec/name/2?category_code=${category_code}&name=${name}`,
      {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification(
        "success",
        "更新規格(二)名稱",
        `編號:${category_code},值:${name}`
      );
    }
  } catch (error) {
    console.log(error);
    Notification("error", "更新規格(二)名稱失敗", `訊息: ${error}`);
  }
};

export const DeleteKindGroup = async (group_code) => {
  try {
    const res = await fetch(`${api}/groups?group_code=${group_code}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      credentials: "include",
    });
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification("warning", "刪除類別群組", `編號:${group_code}`);
    }
  } catch (error) {
    Notification("error", "刪除類別群組失敗", `訊息: ${error}`);
  }
};

export const DeleteKind = async (kind_code) => {
  try {
    const res = await fetch(`${api}/kinds?kind_code=${kind_code}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      credentials: "include",
    });
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification("warning", "刪除類別", `編號:${kind_code}`);
    }
  } catch (error) {
    console.log(error);
    Notification("error", "刪除類別失敗", `訊息: ${error}`);
  }
};

export const DeleteCategory = async (category_code) => {
  try {
    const res = await fetch(
      `${api}/categories?category_code=${category_code}`,
      {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification("warning", "刪除種類", `編號:${category_code}`);
    }
  } catch (error) {
    console.log(error);
    Notification("error", "刪除種類失敗", `${error}`);
  }
};

export const DeleteSpec_1 = async (targetkey) => {
  try {
    const category_code = targetkey.substring(0, 6);
    const spec_code = targetkey.substring(6, 8);
    const res = await fetch(
      `${api}/spec/value/1?category_code=${category_code}&spec_code=${spec_code}`,
      {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification(
        "warning",
        "刪除規格一",
        `編號:${category_code}_${spec_code}`
      );
    }
  } catch (error) {
    console.log(error);
    Notification("error", "刪除規格一失敗", `${error}`);
  }
};

export const DeleteSpec_2 = async (targetkey) => {
  try {
    const category_code = targetkey.substring(0, 6);
    const spec_code = targetkey.substring(6, 8);
    const res = await fetch(
      `${api}/spec/value/2?category_code=${category_code}&spec_code=${spec_code}`,
      {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification(
        "warning",
        "刪除規格二",
        `編號:${category_code}_${spec_code}`
      );
    }
  } catch (error) {
    console.log(error);
    Notification("error", "刪除規格二失敗", `${error}`);
  }
};

export const GetDiscount = async (category_code) => {
  try {
    const res = await fetch(`${api}/discount?category_code=${category_code}`);
    const jsonData = await handleGetData(res);
    // console.log(jsonData);
    return jsonData;
  } catch (error) {
    console.log(error);
    Notification("error", "取得折數失敗", `訊息: ${error}`);
  }
};

export const UpdateCategoryDiscount = async (category_code, discount) => {
  try {
    const res = await fetch(
      `${api}/discount?category_code=${category_code}&discount=${discount}`,
      {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification(
        "success",
        "更新折數",
        `種類:${category_code} 折數:${discount}`
      );
    }

    return res_text;
  } catch (error) {
    console.log(error);
    Notification("error", "更新折數失敗", `${error}`);
  }
};

export const UpdatePrice = async (barcode, price, discount) => {
  try {
    const res = await fetch(
      `${api}/price?barcode=${barcode}&price=${price}&discount=${discount}`,
      {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      }
    );
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification(
        "success",
        "更新價格",
        `條碼:${barcode} 價格:${price} 折數:${discount}`
      );
    }

    return res_text;
  } catch (error) {
    console.log(error);
    Notification("error", "更新價格失敗", `${error}`);
  }
};

export const DeleteImage = async (code, type) => {
  try {
    const res = await fetch(`${api}/shop/image?code=${code}&type=${type}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      credentials: "include",
    });
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification("warning", "刪除圖片", `條碼:${code} 形式:${type}`);
    }
  } catch (error) {
    console.log(error);
    Notification("error", "刪除圖片失敗", `${error}`);
  }
};

export const DeleteFile = async (code, type) => {
  try {
    const res = await fetch(`${api}/shop/file?code=${code}&type=${type}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      credentials: "include",
    });
    let res_text = await handleEditData(res);
    if (res_text) {
      Notification("warning", "刪除文件", `條碼:${code} 形式:${type}`);
    }
  } catch (error) {
    console.log(error);
    Notification("error", "刪除文件失敗", `${error}`);
  }
};

// export const Updatedb = async () => {
//   try {
//     const res = await fetch(`${http}://${host}${api}/updatedb`, {
//       method: "PATCH",
//       headers: { "Content-Type": "application/json" },
//     });
//     Notification("success", "更新資料庫中", `請稍後重新整理`);
//     return res.text();
//   } catch (error) {
//     console.log(error);
//     Notification("error", "更新資料庫失敗", `${error}`);
//   }
// };
