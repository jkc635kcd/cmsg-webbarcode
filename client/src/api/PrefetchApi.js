import {
  GetKinds,
  GetCategories,
  GetSpec_1,
  GetSpec_2,
  GetSpec_1_name,
  GetSpec_2_name,
  GetAllBarcodes,
  GetKindgroups,
  GetDiscount,
} from "./FetchData";

export function fetchProductData() {
  let productPromise = fetchProduct();
  return {
    product: wrapPromise(productPromise),
  };
}

// 處理Promise物件
function wrapPromise(promise) {
  let status = "pending";
  let result;
  let suspender = promise.then(
    (r) => {
      console.log("fetching data end.");
      status = "success";
      result = r;
    },
    (e) => {
      console.error("fetching error.");
      status = "error";
      result = e;
    }
  );
  return {
    read() {
      if (status === "pending") {
        throw suspender;
      } else if (status === "error") {
        throw result;
      } else if (status === "success") {
        return result;
      }
    },
  };
}

// fetch 商品資料，回傳Promise
function fetchProduct() {
  console.log("fetching product data...");

  return new Promise((resolve) => {
    // 定義資料物件
    const origin = {
      groups: [],
      kinds: [],
      categories: [],
      sepc_1: [],
      spec_2: [],
      spec_1_name: "",
      spec_2_name: "",
      barcode_price: {},
      discount: 1,
    };

    // 取得hash作為記錄目前選擇資料
    let hash =
      window.location.hash === ""
        ? "001001"
        : window.location.hash.substring(1);
    let pagination = window.innerWidth > 1500 ? 8 : 5;
    GetKindgroups().then((data) => (origin.groups = data));
    GetKinds(hash.substring(0, 1)).then((data) => (origin.kinds = data));
    GetCategories(hash.substring(0, 3)).then(
      (data) => (origin.categories = data)
    );
    GetSpec_1(hash.substring(0, 6), 1, pagination).then(
      (data) => (origin.spec_1 = data)
    );
    GetSpec_1_name(hash.substring(0, 6))
      .then(
        (data) =>
          (origin.spec_1_name =
            data === "" || data === undefined ? undefined : data[0]["name"])
      )
      .catch((error) => console.log(error));
    GetSpec_2(hash.substring(0, 6)).then((data) => (origin.spec_2 = data));
    GetSpec_2_name(hash.substring(0, 6))
      .then(
        (data) =>
          (origin.spec_2_name =
            data === "" || data === undefined ? undefined : data[0]["name"])
      )
      .catch((error) => console.log(error));
    GetAllBarcodes(hash.substring(0, 6)).then(
      (data) => (origin.barcode_price = data)
    );
    GetDiscount(hash.substring(0, 6))
      .then(
        (data) =>
          (origin.discount =
            data === "" || data === undefined ? 1 : data[0]["discount"])
      )
      .catch((error) => console.log(error));

    // 回傳物件
    resolve(origin);
  });
}
