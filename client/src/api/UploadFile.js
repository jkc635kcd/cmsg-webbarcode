import React, { Fragment } from "react";
import { Upload, message } from "antd";
import { FileImageOutlined, InboxOutlined } from "@ant-design/icons";

const { Dragger } = Upload;

const host = window.location.hostname;
const http = process.env.REACT_APP_API_SSL == true ? "https" : "http";
const port = process.env.REACT_APP_API_PORT;
const api = process.env.REACT_APP_API_VERSION;

export const UploadImage = (props) => {
  const uploadSpecProps = {
    name: "image",
    multiple: false,
    // action: `${http}://${host}${api}/shop/image?code=${props.barcode}&type=spec&permissions=755`,
    listType: "picture",
    showUploadList: false,
    maxCount: 1,
    withCredentials: true,
    onChange(info) {
      console.log(info);
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        message.success(`${info.file.name} 檔案上傳成功`);
        props.onUpload();
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} 檔案上傳失敗`);
        props.onUpload();
      }
    },
  };
  return (
    <Fragment>
      <Dragger {...uploadSpecProps}>
        <p className="ant-upload-drag-icon">
          <FileImageOutlined />
        </p>
        <p className="ant-upload-text">點擊或拖曳圖片至此上傳</p>
        <p className="ant-upload-hint">僅接受 jpg/jpeg/png 檔案類型</p>
      </Dragger>
    </Fragment>
  );
};

export const UploadFile = (props) => {
  const uploadDocumentProps = {
    name: "file",
    multiple: false,
    // action: `${http}://${host}${api}/shop/file?code=${props.barcode}&type=spec&permissions=755`,
    listType: "picture-card",
    showUploadList: false,
    maxCount: 1,
    withCredentials: true,
    onChange(info) {
      console.log(info);
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        message.success(`${info.file.name} 檔案上傳成功`);
        // props.onUpload();
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} 檔案上傳失敗`);
        // props.onUpload();
      }
    },
  };
  return (
    <Fragment>
      <Dragger {...uploadDocumentProps}>
        <p className="ant-upload-drag-icon">
          <InboxOutlined />
        </p>
        <p className="ant-upload-text">點擊或拖曳檔案至此上傳</p>
        <p className="ant-upload-hint">僅接受 pdf 檔案類型</p>
      </Dragger>
    </Fragment>
  );
};
