import React, { Fragment } from "react";
import Router from "./router/router";
import Navbar from "./components/Navbar/Navbar-ant";
import { BackTop } from "antd";
import "./App.css";

function App() {
  return (
    <Fragment>
      <div className="container">
        {/* <Navbar /> */}
        <BackTop />
        <Router />
      </div>
    </Fragment>
  );
}

export default App;
