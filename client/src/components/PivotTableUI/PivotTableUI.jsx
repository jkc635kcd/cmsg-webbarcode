import React, { Fragment, useEffect, useState, useRef } from "react";
import { Tabs, Pagination, Drawer, Empty, Button } from "antd";
import { FormOutlined } from "@ant-design/icons";
import style from "./PivotTable.module.css";
import { Notification } from "../Notification";
import BarcodeTable from "../Table/BarcodeTable";
import PivotTable_LP from "../Table/PivotTable_LP";
import {
  GetKindgroups,
  GetKinds,
  GetCategories,
  GetSpec_1,
  GetSpec_2,
  GetSpec_1_name,
  GetSpec_2_name,
  GetBarcodes,
  GetDiscount,
  CreateKindGroup,
  CreateKind,
  CreateCategory,
  CreateSpec_1,
  CreateSpec_2,
  UpdateKindGroup,
  UpdateKind,
  UpdateCategory,
  UpdateSpec_1,
  UpdateSpec_2,
  UpdateSpec_1_name,
  UpdateSpec_2_name,
  UpdatePrice,
  UpdateCategoryDiscount,
  DeleteKindGroup,
  DeleteKind,
  DeleteCategory,
  DeleteSpec_1,
  DeleteSpec_2,
  DeleteFile,
} from "../../api/FetchData";
import { showPromiseConfirm, showDeleteConfirm, showError } from "../Modal";
import EditModal from "../EditModal";
import { SettingTable } from "../Setting/Setting";
import { useHotkeys } from "react-hotkeys-hook";
import { UploadFile } from "../../api/UploadFile";
import UserService from "../../services/user.service";
const { TabPane } = Tabs;
// window.render_count = 0;

/**
 *
 * @param {Object} props prefetch all data
 */

const PivotTableUI = (props) => {
  // window.render_count += 1;
  // console.log(window.render_count);
  /**
   * kinds: 類別資料, kind_code: 類別代號, kinds_tabNum: 類別tab數量
   * categories: 種類資料, category_code: 種類代號, categories_tabNum: 種類tab數量
   * spec_1: 規格一資料, spec_1_code: 規格一代號, spec_1_tabNum 規格一tab數量
   * spec_2: 規格二資料, spec_2_code: 規格二代號, spec_2_tabNum 規格二tab數量
   * price: 價格資料
   * barcode: 條碼資料
   * barcode_price: 條碼價格資料
   * discount: 折數
   * editable: 編輯模式/檢視模式狀態
   * host: client端網站ip位址
   */
  // console.log(props.data.groups);
  // console.log(props.data.kinds);
  // console.log(props.data.categories);
  // console.log(props.data.spec_1);
  // console.log(props.data.spec_1_name);
  // console.log(props.data.spec_2);
  // console.log(props.data.spec_2_name);
  // console.log(props.data.barcode_price);
  // console.log(props.data.discount);
  let hash =
    window.location.hash === "" ? "001001" : window.location.hash.substring(1);
  const href = window.location.pathname.split("/")[1];
  let fixedPagination = window.innerWidth > 1500 ? 8 : 5;
  const [groups, setGroups] = useState(props.data.groups);
  const [group_code, setGroupCode] = useState(hash.substring(0, 1));
  const [kinds, setKinds] = useState(props.data.kinds);
  const [kind_code, setKindCode] = useState(hash.substring(0, 3));
  const [kinds_tabNum, setKindsTabNum] = useState(0);
  const [categories, setCategories] = useState(props.data.categories);
  const [category_code, setCategoryCode] = useState(hash.substring(0, 6));
  const [categories_tabNum, setCategoriesTabNum] = useState([]);
  const [spec_1, setSpec_1] = useState(props.data.spec_1);
  const [spec_1_code, setSpec_1_code] = useState("01");
  const [spec_1_tabNum, setSpec_1_TabNum] = useState(0);
  const [spec_1_var, setSpec_1_var] = useState("");
  const [spec_2, setSpec_2] = useState(props.data.spec_2);
  const [spec_2_code, setSpec_2_code] = useState("01");
  const [spec_2_tabNum, setSpec_2_TabNum] = useState(0);
  const [spec_2_var, setSpec_2_var] = useState("");
  const [barcode, setBarcode] = useState(`99${hash.substring(0, 6)}0101`);
  const [barcode_price, setBarcodePrice] = useState(props.data.barcode_price);
  const [editable, setEditable] = useState(false);
  const [spec_1_name, setSpec_1_name] = useState(props.data.spec_1_name);
  const [spec_2_name, setSpec_2_name] = useState(props.data.spec_2_name);
  const [discount, setDiscount] = useState(props.data.discount);
  const [groupName, setGroupName] = useState({});
  const [kindName, setKindName] = useState({});
  const [categoryName, setCategoryName] = useState({});
  const [page, setPage] = useState(1);
  const [infoDrawer, setInfoDrawer] = useState(false);

  // 修改參數資料視窗
  const [modalVisible, setModalVisible] = useState(false);
  const [modalData, setModalData] = useState({});
  // 修改參數名稱視窗
  const [updateVisible, setUpdateVisible] = useState(false);
  // 規格高亮參數
  const [lightTh1, setLightTh1] = useState("01");
  const [lightTh2, setLightTh2] = useState("01");

  const host =
    window.location.hostname === "localhost"
      ? process.env.REACT_APP_FTP_HOST
      : window.location.hostname;
  const http = process.env.REACT_APP_FTP_SSL === true ? "https" : "http";
  const nas = process.env.REACT_APP_NAS_PATH;
  const ssid = process.env.REACT_APP_FTP_SHARE_SSID;
  const fid = process.env.REACT_APP_FTP_SHARE_SSID;
  const pwd = process.env.REACT_APP_FTP_SHARE_PWD;

  // 切換編輯環境熱鍵
  useHotkeys("e", () => handleVerifyEdit());
  useHotkeys("q", () => setEditable(false));

  /**
   * fetch API 取得資料
   */
  const getGroups = () => {
    GetKindgroups()
      .then((data) => setGroups(data))
      .catch((error) => console.log(error));
  };

  const getKinds = (group_code) => {
    GetKinds(group_code)
      .then((data) => setKinds(data))
      .catch((error) => console.log(error));
  };

  const getCategories = (kind_code) => {
    GetCategories(kind_code)
      .then((data) => setCategories(data))
      .catch((error) => console.log(error));
  };

  const getSpec_1 = (category_code, page, pagination) => {
    GetSpec_1(category_code, page, pagination)
      .then((data) => setSpec_1(data))
      .catch((error) => console.log(error));
  };

  const getSpec_2 = (category_code) => {
    GetSpec_2(category_code)
      .then((data) => setSpec_2(data))
      .catch((error) => console.log(error));
  };

  const getSpec_1_name = (category_code) => {
    GetSpec_1_name(category_code)
      .then((data) =>
        setSpec_1_name(
          data === "" || data === undefined ? undefined : data[0]["name"]
        )
      )
      .catch((error) => console.log(error));
  };

  const getSpec_2_name = (category_code) => {
    GetSpec_2_name(category_code)
      .then((data) =>
        setSpec_2_name(
          data === "" || data === undefined ? undefined : data[0]["name"]
        )
      )
      .catch((error) => console.log(error));
  };

  const getBarcodes = (category_code, page, pagination) => {
    GetBarcodes(category_code, page, pagination)
      .then((data) => setBarcodePrice(data))
      .catch((error) => console.log(error));
  };

  const getDiscount = (category_code) => {
    GetDiscount(category_code)
      .then((data) =>
        setDiscount(data === "" || data === undefined ? 1 : data[0]["discount"])
      )
      .catch((error) => console.log(error));
  };

  /**
   * @param {string} code group code.
   * */

  const group_onChange = (code) => {
    setGroupCode(code);
    getKinds(code);
    kind_onChange(code + "01");
  };

  const kind_onChange = (code) => {
    setKindCode(code);
    getCategories(code);
    setCategoryCode(code + "001");
    category_onChange(code + "001");
    window.location.hash = `#${code}001`;
    clearHighlight();
  };

  const category_onChange = (code) => {
    setCategoryCode(code);
    getSpec_1(code, 1, fixedPagination);
    getSpec_2(code);
    getSpec_1_name(code);
    getSpec_2_name(code);
    getBarcodes(code, 1, fixedPagination);
    setSpec_1_code("01");
    setSpec_2_code("01");
    setSpec_1_var("");
    setSpec_2_var("");
    window.location.hash = `#${code}`;
    setBarcode(`99${code}0101`);
    setLightTh1("01");
    setLightTh2("01");
    clearHighlight();
    setPage(1);
    fadeTable();
    fadeImg();
  };

  const spec_1_onChange = (code) => {
    setSpec_1_code(code);
    setBarcode(`99${category_code}${code}${spec_2_code}`);
    getBarcodes(category_code, page, fixedPagination);
    fadeImg();
  };

  const spec_2_onChange = (code) => {
    setSpec_2_code(code);
    setBarcode(`99${category_code}${spec_1_code}${code}`);
    getBarcodes(category_code, page, fixedPagination);
    fadeImg();
  };

  const reloadOnDelete = () => {
    getGroups();
    getKinds(group_code);
    getCategories(kind_code);
    getSpec_1(category_code, page, fixedPagination);
    getSpec_2(category_code);
  };

  // 資料刪除處理
  const handleDelete = (props) => {
    try {
      const title = props.title;
      const content = props.content;
      const api = props.api;
      const targetkey = props.targetkey;
      const reload = props.reload;
      showDeleteConfirm(title, content, api, targetkey, reload);
    } catch (error) {
      console.error(error);
      showError(error);
    }
  };

  let event = {
    title: undefined,
    content: undefined,
    api: undefined,
    targetkey: undefined,
  };

  /**
   * @param {number} targetkey
   * @param {string} action
   */
  const group_onEdit = (targetkey, action, e) => {
    switch (action) {
      case "remove":
        // console.log(`remove ${targetkey}`);
        event.title = `刪除類別群組 ${targetkey}`;
        event.content = (
          <div>刪除群組會將底下關聯的資料一併刪除，確認是否刪除 ?</div>
        );
        event.api = DeleteKindGroup;
        event.targetkey = targetkey;
        event.reload = () => reloadOnDelete();
        handleDelete(event);
        break;
      case "add":
        // console.log(`add ${groups_tabNum}`);
        setModalVisible(true);
        setModalData({
          title: "新增類別群組",
          content:
            "可新增0~9不重複共10種不同類別的群組。 例: 五金材料、電動工具...等。",
          type: "group",
          addHook: CreateKindGroup,
          reload: () => getGroups(),
        });
        break;
      default:
        console.log("wrong action");
        break;
    }
  };

  /**
   * @param {number} targetkey
   * @param {string} action
   */
  const kind_onEdit = (targetkey, action, e) => {
    switch (action) {
      case "remove":
        // console.log(`remove ${targetkey}`);
        event.title = `刪除類別 ${targetkey}`;
        event.content = (
          <div>刪除類別會將底下關聯的資料一併刪除，確認是否刪除 ?</div>
        );
        event.api = DeleteKind;
        event.targetkey = targetkey;
        event.reload = () => reloadOnDelete();
        handleDelete(event);
        setKindsTabNum(kinds_tabNum - 1);
        getKinds(group_code);
        break;
      case "add":
        // console.log(`add ${kinds_tabNum + 1}`);
        setModalVisible(true);
        console.log(group_code);
        setModalData({
          title: "新增類別",
          content:
            "可新增1~99不重複共99種不同的類別。 例: 螺絲、刀片、水管、抽水機、割草機...等。",
          type: "kind",
          target: group_code,
          addHook: CreateKind,
          reload: () => getKinds(group_code),
        });
        break;
      default:
        console.log("wrong action");
        break;
    }
  };
  /**
   * @param {number} targetkey
   * @param {string} action
   */
  const category_onEdit = (targetkey, action, e) => {
    switch (action) {
      case "remove":
        // console.log(`remove ${targetkey}`);
        event.title = `刪除種類 ${targetkey}`;
        event.content = (
          <div>刪除種類會將底下關聯的資料一併刪除，確認是否刪除 ?</div>
        );
        event.api = DeleteCategory;
        event.targetkey = targetkey;
        event.reload = () => reloadOnDelete();
        handleDelete(event);
        setCategoriesTabNum(categories_tabNum - 1);
        break;
      case "add":
        // console.log(`add ${categories_tabNum + 1}`);
        setModalVisible(true);
        setModalData({
          title: "新增種類",
          content:
            "可新增1~999不重複共999種不同的種類。 例: 六角螺絲、十字螺絲、WORX...等。",
          type: "category",
          target: kind_code,
          addHook: CreateCategory,
          reload: () => {
            getCategories(kind_code);
            getSpec_1_name(category_code);
            getSpec_2_name(category_code);
          },
        });
        break;
      default:
        console.log("wrong action");
        break;
    }
  };
  /**
   * @param {number} targetkey
   * @param {string} action
   */
  const spec_1_onEdit = (targetkey, action, e) => {
    e.stopPropagation();
    switch (action) {
      case "remove":
        // console.log(`remove ${targetkey}`);
        event.title = `刪除規格(欄) ${targetkey}`;
        event.content = (
          <div>刪除規格會將同一欄/列相關的資料一併刪除，確認是否刪除 ?</div>
        );
        event.api = DeleteSpec_1;
        event.targetkey = category_code + targetkey;
        event.reload = () => getSpec_1(category_code, page, fixedPagination);
        handleDelete(event);
        clearHighlight();
        setSpec_1_TabNum(spec_1_tabNum - 1);
        break;
      case "add":
        // console.log(`add ${spec_1_tabNum + 1}`);
        setModalVisible(true);
        setModalData({
          title: "新增規格(欄)",
          content:
            "可新增1~99不重複共99種不同的規格。 例: 3cm、5cm、WORX...等。",
          type: "spec",
          target: category_code,
          addHook: CreateSpec_1,
          reload: () => {
            getSpec_1(category_code, page, fixedPagination);
            getBarcodes(category_code, page, fixedPagination);
          },
        });
        clearHighlight();
        break;
      case "update":
        // console.log(`update spec_1 ${targetkey}`);
        setModalVisible(true);
        setModalData({
          title: `更新規格(一) ${targetkey}`,
          content: "請輸入欲修改的名稱。",
          type: "spec",
          action: action,
          addHook: (res) => UpdateSpec_1(category_code, targetkey, res),
          reload: () => {
            getSpec_1(category_code, page, fixedPagination);
            getBarcodes(category_code, page, fixedPagination);
          },
        });
        break;
      default:
        console.log("wrong action");
        break;
    }
  };
  /**
   * @param {number} targetkey
   * @param {string} action
   */
  const spec_2_onEdit = (targetkey, action, e) => {
    e.stopPropagation();
    switch (action) {
      case "remove":
        // console.log(`remove ${targetkey}`);
        event.title = `刪除規格(列) ${targetkey}`;
        event.content = (
          <div>刪除規格會將同一欄/列相關的資料一併刪除，確認是否刪除 ?</div>
        );
        event.api = DeleteSpec_2;
        event.targetkey = category_code + targetkey;
        event.reload = () => getSpec_2(category_code);
        handleDelete(event);
        clearHighlight();
        setSpec_2_TabNum(spec_2_tabNum - 1);
        break;
      case "add":
        // console.log(`add ${spec_2_tabNum + 1}`);
        setModalVisible(true);
        setModalData({
          title: "新增規格(列)",
          content:
            "可新增1~99不重複共99種不同的規格。 例: 3cm、5cm、WORX...等。",
          type: "spec",
          target: category_code,
          addHook: CreateSpec_2,
          reload: () => {
            getSpec_2(category_code);
            getBarcodes(category_code, page, fixedPagination);
          },
        });
        clearHighlight();
        break;
      case "update":
        // console.log(`update spec_2 ${targetkey}`);
        setModalVisible(true);
        setModalData({
          title: `更新規格(二) ${targetkey}`,
          content: "請輸入欲修改的名稱。",
          type: "spec",
          action: action,
          addHook: (res) => UpdateSpec_2(category_code, targetkey, res),
          reload: () => {
            getSpec_2(category_code, page, fixedPagination);
            getBarcodes(category_code, page, fixedPagination);
          },
        });
        break;
      default:
        console.log("wrong action");
        break;
    }
  };

  //價錢設定
  const price_onEdit = async (barcode, value, discount) => {
    let reg = /^\d+$/;
    // if (price === value) {
    //   console.log("no action");
    //   return;
    // }
    if (reg.test(value)) {
      let res = await UpdatePrice(barcode, value, discount);
      console.log(res);
    } else {
      console.log(`only input number`);
      Notification("error", "價格錯誤", "只能輸入數字");
    }
  };

  //折數設定
  const discount_set = async (v) => {
    await UpdateCategoryDiscount(category_code, v);
  };

  //內容修改設定

  //高亮選擇的結果
  const setColumnHighlight = (sp1, sp2, sp1_var, sp2_var) => {
    let spec1_th = document.getElementById(`th1_${sp1}`);
    let spec2_th = document.getElementById(`th2_${sp2}`);
    let column = document.getElementById(sp1 + sp2);

    clearHighlight();
    if (sp1_var !== "") {
      // 高亮整行
      for (let i = 1; i < spec_2.length + 1; i++) {
        let a = i;
        a >= 10 ? (a = String(i)) : (a = "0" + i);
        let c = document.getElementById(`${sp1}${a}`);
        c.style.backgroundColor = "#bee5ff77";
        spec1_th.style.backgroundColor = "#bee5ff77";
      }
    }
    if (sp2_var !== "") {
      // 高亮整列
      for (let i = 1; i < spec_1["rows"].length + 1; i++) {
        let a = i;
        a >= 10 ? (a = String(i)) : (a = "0" + i);

        let c = document.getElementById(`${a}${sp2}`);
        c.style.backgroundColor = "#ffe2e477";
        spec2_th.style.backgroundColor = "#ffe2e477";
      }
    }
    if (sp1_var !== "" && sp2_var !== "") {
      column.style.backgroundColor = "yellow";
    }
  };

  //清除選擇高亮
  const clearHighlight = () => {
    let th = document.getElementById("PivotTable").getElementsByTagName("th");
    let td = document.getElementById("PivotTable").getElementsByTagName("td");
    for (let i = 0; i < td.length; i++) {
      const element = td[i];
      element.style.backgroundColor = "";
    }
    for (let i = 0; i < th.length; i++) {
      const element = th[i];
      element.style.backgroundColor = "#cbeed7";
    }
  };

  //前往表格分頁
  const goPage = (currentPage) => {
    setPage(currentPage);
    getSpec_1(category_code, currentPage, fixedPagination);
    getBarcodes(category_code, currentPage, fixedPagination);
    setSpec_1_var("");
    setSpec_2_var("");
    setSpec_1_code("");
    setSpec_2_code("");
    setLightTh1("01");
    setLightTh2("01");
    clearHighlight();
    fadeTable();
    fadeImg();
  };

  const fadeTable = () => {
    let table = document.getElementById("PivotTable");
    if (table !== null) {
      table.style.animation = "none";
      setTimeout(() => {
        table.style.animation = null;
      }, 100);
    }
  };

  const fadeImg = () => {
    try {
      let img =
      document.getElementById("barcode_table").parentElement.parentElement
        .parentElement;
    console.log(img);
    if (img !== null) {
      img.style.animation = "none";
      setTimeout(() => {
        img.style.animation = null;
      }, 200);
    }
    } catch (error) {
      console.error(error);
    }
  };

  const handleVerifyEdit = () => {
    UserService.getModerator()
      .then((res) => {
        console.log(res);
        setEditable(!editable);
      })
      .catch((error) => {
        console.log(error);
        alert("權限不符!");
      });
  };

  // 停止刷新網頁時的初次渲染，當指定的值改變時再渲染
  const useDidMountEffect = (func, deps) => {
    const didMount = useRef(false);

    useEffect(() => {
      if (didMount.current) func();
      else didMount.current = true;
    }, deps);
  };

  /**網頁重整或資料變動時重新render畫面 */

  // groups 變化時更新groups tab
  useEffect(() => {
    group_onChange(
      window.location.hash === "" ? "0" : window.location.hash.substring(1, 2)
    );
    // setPageSize(spec_1.length);
    // getDiscount(category_code);
  }, [groups]);

  // kinds tab 新增或刪除時更新資料
  // useDidMountEffect(() => {
  //   getKinds(group_code);
  // }, [kinds_tabNum]);

  // kinds 變化時更新kinds tab
  useEffect(() => {
    setKindsTabNum(kinds.length);
    // setPageSize(spec_1.length);
    // getDiscount(category_code);
  }, [kinds]);

  // categories tab 新增或刪除時更新資料
  // useDidMountEffect(() => {
  //   getCategories(kind_code);
  //   console.log("test1");
  // }, [categories_tabNum]);

  // categories 變化時更新categories tab
  useEffect(() => {
    setCategoriesTabNum(categories.length);
  }, [categories]);

  // spec_1 tab 新增或刪除時更新資料
  // useDidMountEffect(() => {
  //   getSpec_1(category_code);
  //   console.log("test3");
  // }, [spec_1_tabNum]);

  // spec_1 變化時更新spec_1 tab
  useEffect(() => {
    if(!spec_1) return;
    setSpec_1_TabNum(spec_1["count"]);
    getDiscount(category_code);
  }, [spec_1]);

  // spec_2 tab 新增或刪除時更新資料
  // useDidMountEffect(() => {
  //   getSpec_2(category_code);
  // }, [spec_2_tabNum]);

  // spec_2 變化時更新spec_2 tab
  useEffect(() => {
    try {
      setSpec_2_TabNum(spec_2.length);
      let gName = {};
      let kName = {};
      let cName = {};
      for (const key in groups) {
        if (groups.hasOwnProperty(key)) {
          const element = groups[key];
          gName[element.group_code] = element.group_name;
          setGroupName(gName);
        }
      }
      for (const key in kinds) {
        if (kinds.hasOwnProperty(key)) {
          const element = kinds[key];
          kName[element.kind_code] = element.kind_name;
          setKindName(kName);
        }
      }
      for (const key in categories) {
        if (categories.hasOwnProperty(key)) {
          const element = categories[key];
          cName[element.category_code] = element.category_name;
          setCategoryName(cName);
        }
      }
    } catch (error) {
      console.error(error);
    }
  }, [spec_2]);

  // refresh時執行一次
  // useDidMountEffect(() => {
  //   getGroups();
  // }, []);
  // useDidMountEffect(() => {
  //   category_onChange(hash.substring(0, 6));
  // }, []);
  // useDidMountEffect(() => {
  //   // setBarcode("990010010101");
  //   setBarcode(`99${hash.substring(0, 6)}}0101`);
  // }, []);

  return (
    <Fragment>
      <Button
        type={"dashed"}
        onClick={handleVerifyEdit}
        icon={<FormOutlined />}
        className={style.editmode}
      >
        切換模式
      </Button>
      {/* 類別群組選擇列 */}
      <Tabs
        type={editable ? "editable-card" : "line"}
        hideAdd={editable ? false : true}
        defaultActiveKey={group_code}
        onChange={group_onChange}
        onEdit={group_onEdit}
        className={style.tab}
        style={{ color: "#E03C8A" }}
        animated={{ inkBar: false, tabPane: false }}
      >
        {groups === undefined
          ? null
          : [...Array.from(groups, (v, i) => v)].map((v, i) => (
              <TabPane
                tab={
                  <span>
                    {`${v.group_name}` + (editable ? `(${v.group_code})` : "")}
                  </span>
                }
                key={v.group_code}
              ></TabPane>
            ))}
      </Tabs>
      {/* 類別選擇列 */}
      <Tabs
        type={editable ? "editable-card" : "card"}
        hideAdd={editable ? false : true}
        defaultActiveKey="0"
        activeKey={kind_code}
        onChange={kind_onChange}
        tabPosition={"top"}
        onEdit={kind_onEdit}
        className={style.tab}
        style={{ color: "#F05E1C" }}
        animated={{ inkBar: false, tabPane: false }}
      >
        {kinds === undefined
          ? null
          : [...Array.from(kinds, (v, i) => v)].map((v, i) => (
              <TabPane
                tab={
                  <span>
                    {`${v.kind_name}` + (editable ? `(${v.kind_code})` : "")}
                  </span>
                }
                key={v.kind_code}
              ></TabPane>
            ))}
      </Tabs>
      {/* 種類選擇列 */}
      <Tabs
        type={editable ? "editable-card" : "card"}
        hideAdd={editable ? false : true}
        defaultActiveKey="0"
        activeKey={category_code}
        onChange={category_onChange}
        tabPosition={"top"}
        onEdit={category_onEdit}
        className={style.tab}
        style={{ color: "#66327C" }}
        animated={{ inkBar: false, tabPane: false }}
      >
        {categories === undefined
          ? null
          : [...Array.from(categories, (v, i) => v)].map((v, i) => (
              <TabPane
                tab={
                  <span>
                    {`${v.category_name}` +
                      (editable ? `(${v.category_code})` : "")}
                  </span>
                }
                key={v.category_code}
              ></TabPane>
            ))}
      </Tabs>
      {/* 樞紐分析表與條碼欄位 */}
      <div>
        <div className={style.div_pivot}>
          {spec_1 === "" ||
          spec_1 === undefined ||
          spec_2 === "" ||
          spec_2 === undefined ? null : (
            <div style={{ display: "flex" }}>
              <div>
                <Pagination
                  defaultCurrent={1}
                  current={page}
                  defaultPageSize={fixedPagination}
                  total={spec_1 ? spec_1["count"]:0}
                  onChange={(page) => goPage(page)}
                  showQuickJumper
                  showSizeChanger={false}
                  style={{ marginLeft: "3em" }}
                />
              </div>
              {editable ? (
                <Button type="primary" onClick={() => setUpdateVisible(true)}>
                  編輯資料名稱
                </Button>
              ) : null}
            </div>
          )}

          {categories === "" || categories === undefined ? (
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
          ) : null}
          <PivotTable_LP
            spec_1_name={spec_1_name}
            spec_2_name={spec_2_name}
            spec_1={
              spec_1 === "" || spec_1 === undefined ? undefined : spec_1["rows"]
            }
            spec_2={spec_2}
            categories={categories}
            spec_1_onChange={spec_1_onChange}
            spec_1_onEdit={spec_1_onEdit}
            setSpec_1_var={setSpec_1_var}
            spec_2_onChange={spec_2_onChange}
            spec_2_onEdit={spec_2_onEdit}
            setSpec_2_var={setSpec_2_var}
            category_code={category_code}
            editable={editable}
            price_onEdit={price_onEdit}
            barcode_price={barcode_price}
            discount={discount}
            onClickSpec1={(th1) => {
              setLightTh1(th1[0]);
              setColumnHighlight(th1[0], lightTh2, th1[1], spec_2_var);
            }}
            onClickSpec2={(th2) => {
              setLightTh2(th2[0]);
              setColumnHighlight(lightTh1, th2[0], spec_1_var, th2[1]);
            }}
          />
          {spec_1 === "" ||
          spec_1 === undefined ||
          spec_2 === "" ||
          spec_2 === undefined ? null : (
            <BarcodeTable
              barcode={barcode}
              price={
                barcode_price === {}
                  ? null
                  : Math.round(barcode_price[barcode] * discount)
              }
              discount={discount}
              group_code={group_code}
              kind_code={kind_code}
              category_code={category_code}
              spec_1_name={spec_1_name}
              spec_2_name={spec_2_name}
              spec_1={spec_1_var}
              spec_2={spec_2_var}
              imgName={
                group_code === "0" ? "0000" : `${spec_1_code}${spec_2_code}`
              }
              groupName={groupName}
              kindName={kindName}
              categoryName={categoryName}
              setInfoDrawer={setInfoDrawer}
              href={href}
              editable={editable}
              onUpload={() => getBarcodes(category_code, page, fixedPagination)}
              onDelete={() => getBarcodes(category_code, page, fixedPagination)}
            />
          )}
        </div>
      </div>
      {/* 參數設定功能: 折數設定、匯入、匯出... */}
      <div
        className={style.div_mode}
        style={{ display: href === "pivot_edit" ? "block" : "none" }}
      ></div>
      {/* 商品資訊欄 */}
      <Drawer
        title={
          <div>
            商品資訊
            {/* <Button type={"dashed"} onClick={handleVerifyEdit}>
              切換模式
            </Button> */}
            {editable ? (
              <Button
                type={"primary"}
                onClick={() =>
                  showDeleteConfirm(
                    "刪除文件",
                    <div>是否刪除商品文件 ?</div>,
                    DeleteFile,
                    barcode,
                    () => null
                  )
                }
                danger
              >
                刪除文件
              </Button>
            ) : null}
          </div>
        }
        placement="left"
        closable={true}
        onClose={() => setInfoDrawer(false)}
        visible={infoDrawer}
        width={window.innerWidth > 1280 ? "50%" : "100%"}
      >
        {editable ? (
          <div style={{ height: "20vh" }}>
            <UploadFile {...{ barcode: barcode }} />
          </div>
        ) : null}
        {/* <iframe
          src={`${http}://${host}${nas}?ssid=${ssid}&fid=${fid}&openfolder=normal&path=/${group_code}/${kind_code}/${category_code}/pdf/${barcode}.pdf&ep=${pwd}&time=${Date.now()}`}
          width="100%"
          height="100%"
          frameBorder={0}
        ></iframe> */}
      </Drawer>
      {/* 新增/修改視窗 */}
      <EditModal
        visible={modalVisible}
        title={modalData.title}
        type={modalData.type}
        action={modalData.action}
        content={modalData.content}
        onCancel={() => setModalVisible(false)}
        target={modalData.target}
        name={modalData.name}
        addHook={modalData.addHook}
        onCreate={(e) => {
          setModalVisible(e);
          setTimeout(modalData.reload, 100);
        }}
      />
      <SettingTable
        {...{
          title: "編輯名稱",
          visible: updateVisible,
          onClose: () => setUpdateVisible(false),
          onGroupUpdate: async (res) => {
            await UpdateKindGroup(group_code, res);
            await getGroups();
          },
          onKindUpdate: async (res) => {
            await UpdateKind(kind_code, res);
            await getKinds(group_code);
          },
          onCategoryUpdate: async (res) => {
            await UpdateCategory(category_code, res, 1);
            await getCategories(kind_code);
          },
          onSpec1Update: async (res) => {
            await UpdateSpec_1_name(category_code, res);
            await getSpec_1_name(category_code);
          },
          onSpec2Update: async (res) => {
            await UpdateSpec_2_name(category_code, res);
            await getSpec_2_name(category_code);
          },
          default: {
            group: groupName[group_code],
            kind: kindName[kind_code],
            category: categoryName[category_code],
            spec_1: spec_1_name,
            spec_2: spec_2_name,
          },
        }}
      />
    </Fragment>
  );
};

export default PivotTableUI;
