import React, { Fragment } from "react";
import { UploadOutlined } from "@ant-design/icons";
import { Button, Upload, message, Tabs } from "antd";

const TestPage = () => {
  const { TabPane } = Tabs;
  const props = {
    name: "file",
    action: "http://127.0.0.1:9999/ftp",
    headers: {
      authorization: "authorization-text",
    },
    onChange(info) {
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  const change = () => {};
  return (
    <Fragment>
      <Upload {...props}>
        <Button icon={<UploadOutlined />}>Click to Upload</Button>
      </Upload>
      <Tabs
        type={"line"}
        tabPosition={"top"}
        hideAdd={true}
        defaultActiveKey={0}
        onChange={change}
        style={{ color: "#E03C8A" }}
        animated={{ inkBar: false, tabPane: false }}
      >
        {[...Array.from([...new Array(10)], (v, i) => v)].map((v, i) => (
          <TabPane tab={`${v}`} key={i}></TabPane>
        ))}
      </Tabs>
    </Fragment>
  );
};

export default TestPage;
