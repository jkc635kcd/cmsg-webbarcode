import React from "react";
import { Form, Input, InputNumber, message, Modal } from "antd";
import {
  ExclamationCircleOutlined,
  EditOutlined,
  FieldNumberOutlined,
} from "@ant-design/icons";

// 修改確認視窗
const { info, confirm, error } = Modal;

// 顯示資訊
export const showInfo = (title, messge) => {
  info({
    title: title,
    content: message,
    okText: "確認",
  });
};

// 一般警告資訊
export const showPromiseConfirm = (title, content, api, targetkey) => {
  confirm({
    title: title,
    icon: <ExclamationCircleOutlined />,
    content: content,
    okText: "確認",
    cancelText: "取消",
    okButtonProps: { htmlType: "submit" },
    onOk() {
      return new Promise((resolve, reject) => {
        const res = api(targetkey);
        resolve(res);
      }).catch(() => console.log("Oops errors!"));
    },
  });
};

// 刪除警告資訊
export const showDeleteConfirm = (title, content, api, targetkey, reload) => {
  confirm({
    title: title,
    icon: <ExclamationCircleOutlined />,
    content: content,
    okText: "刪除",
    okType: "danger",
    cancelText: "取消",
    maskClosable: true,
    onOk() {
      return new Promise((resolve, reject) => {
        const res = api(targetkey);
        resolve(res);
        setTimeout(reload, 100);
      }).catch(() => console.log("Oops errors!"));
    },
  });
};

export const showAddConfirm = (title, content, api, targetkey) => {
  return <Modal title={title}>{content}</Modal>;
};

// 錯誤資訊
export const showError = (message) => {
  error({
    title: "錯誤",
    content: message,
    okText: "確認",
  });
};

const Form_AddGroup = (
  <div>
    <div>請輸入0~9未使用群組代號及名稱。</div>
    <Form
      layout={{ labelCol: { span: 8 }, wrapperCol: { span: 16 } }}
      initialValues={{ remember: true }}
      onFinish={(e) => console.log(e)}
      onFinishFailed={(e) => console.log(e)}
    >
      <Form.Item
        label={"群組代號"}
        name={"群組代號"}
        rules={[{ required: true, message: "請輸入代號!" }]}
      >
        <InputNumber
          min={0}
          max={9}
          onChange={(e) => console.log(e)}
          prefix={<FieldNumberOutlined />}
          placeholder={"群組代號"}
        ></InputNumber>
      </Form.Item>
      <Form.Item
        label={"群組名稱"}
        name={"群組名稱"}
        rules={[{ required: true, message: "請輸入群組名稱!" }]}
      >
        <Input
          onChange={(e) => console.log(e.target.value)}
          prefix={<EditOutlined />}
          placeholder={"群組名稱"}
        ></Input>
      </Form.Item>
    </Form>
  </div>
);
