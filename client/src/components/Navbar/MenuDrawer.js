import React, { Component, Fragment, useState, useEffect } from "react";
import { Menu } from "antd";
import {
  HomeOutlined,
  DatabaseOutlined,
  LoginOutlined,
  LogoutOutlined,
  UserOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { getCurrentUser } from "../../services/auth.service";
const SubMenu = Menu.SubMenu;

const MenuDrawer = () => {
  const [path, setPath] = useState("");
  const [text, setText] = useState("");
  useEffect(() => {
    getCurrentUser()
      .then((res) => {
        console.log(res);
        setPath("logout");
        setText("登出");
      })
      .catch((error) => {
        console.log(error);
        setPath("login");
        setText("登入");
      });
  }, []);
  return (
    <Fragment>
      <Menu mode="inline">
        <Menu.Item key="1" icon={<HomeOutlined />}>
          <a href="home">首頁</a>
        </Menu.Item>
        <Menu.Item key="2" icon={<DatabaseOutlined />}>
          <a href="barcode">商品資料庫</a>
        </Menu.Item>
        {/* <Menu.Item key="3">
          <a href="null">未開放</a>
        </Menu.Item> */}

        <SubMenu key="sub1" title="系統" icon={<SettingOutlined />}>
          {path === "login" ? null : (
            <Menu.Item key="4" icon={<UserOutlined />}>
              <a href="/profile">帳號資訊</a>
            </Menu.Item>
          )}
          <Menu.Item
            key="5"
            icon={path === "login" ? <LoginOutlined /> : <LogoutOutlined />}
          >
            <a href={`${path}`}>{`${text}`}</a>
          </Menu.Item>
        </SubMenu>
      </Menu>
    </Fragment>
  );
};
export default MenuDrawer;
