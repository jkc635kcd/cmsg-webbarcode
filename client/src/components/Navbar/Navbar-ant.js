import React, { Component } from "react";
import LeftMenu from "./LeftMenu";
import RigntMenu from "./RightMenu";
import { Drawer, Button, Layout, Menu } from "antd";
import MenuDrawer from "./MenuDrawer";
import style from "./style.module.css";
const { Header, Content, Footer } = Layout;
class Navbar extends Component {
  state = {
    current: "mail",
    visible: false,
  };
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };
  onClose = () => {
    this.setState({
      visible: false,
    });
  };
  render() {
    return (
      <Header className={style.navBar}>
        <div className="menuCon">
          <div className="logo">
            <a href="/home">CMSG</a>
          </div>
          <div className="leftMenu">
            <LeftMenu />
          </div>
          <div className="rightMenu">
            <RigntMenu />
          </div>

          <Drawer
            title="CMSG"
            placement="right"
            closable={false}
            onClose={this.onClose}
            visible={this.state.visible}
          >
            <MenuDrawer />
          </Drawer>
        </div>
        <Button className="barsMenu" type="default" onClick={this.showDrawer}>
          <span className="barsBtn"></span>
        </Button>
      </Header>
    );
  }
}
export default Navbar;
