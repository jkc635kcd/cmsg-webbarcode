import React, { Fragment, useEffect, useState } from "react";
import { Menu, Icon, Avatar } from "antd";
import { UserOutlined, LoginOutlined, LogoutOutlined } from "@ant-design/icons";
import { getCurrentUser } from "../../services/auth.service";
import style from "./style.module.css";
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

const RightMenu = () => {
  const [path, setPath] = useState("");
  const [text, setText] = useState("");
  const [selected, setSelected] = useState("");
  useEffect(() => {
    getCurrentUser()
      .then((res) => {
        console.log(res);
        setPath("logout");
        setText("登出");
      })
      .catch((error) => {
        console.log(error);
        setPath("login");
        setText("登入");
      });

    setSelected(window.location.pathname);
  }, []);
  return (
    <Fragment>
      <Menu
        mode="horizontal"
        theme="dark"
        defaultSelectedKeys={[window.location.pathname]}
        selectedKeys={[selected]}
        className={style.rightMenu}
      >
        {path === "login" ? null : (
          <Menu.Item key="/profile" icon={<UserOutlined />}>
            <a href="/profile">帳號資訊</a>
          </Menu.Item>
        )}
        <Menu.Item
          key={`/${path}`}
          icon={path === "login" ? <LoginOutlined /> : <LogoutOutlined />}
        >
          <a href={`${path}`}>{`${text}`}</a>
        </Menu.Item>
      </Menu>
    </Fragment>
  );
};

export default RightMenu;
