import React, { Fragment, useEffect, useState } from "react";
import { Menu } from "antd";
import { DatabaseOutlined } from "@ant-design/icons";

const LeftMenu = () => {
  const [path, setPath] = useState("");
  useEffect(() => {
    setPath(window.location.pathname);
  }, []);
  return (
    <Fragment>
      <Menu
        mode="horizontal"
        theme="dark"
        defaultSelectedKeys={[window.location.pathname]}
        selectedKeys={[path]}
      >
        <Menu.Item key="/barcode" icon={<DatabaseOutlined />}>
          <a href="barcode">商品資料庫</a>
        </Menu.Item>
        {/* <Menu.Item key="/null">
          <a href="null">未開放</a>
        </Menu.Item> */}
      </Menu>
    </Fragment>
  );
};
export default LeftMenu;
