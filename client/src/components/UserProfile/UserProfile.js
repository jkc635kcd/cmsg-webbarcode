import React, { Fragment, useEffect, useState } from "react";
import { Card, Avatar } from "antd";
import { EditOutlined, SettingOutlined } from "@ant-design/icons";
import style from "./style.module.css";
const { Meta } = Card;

const UserProfile = (props) => {
  const [data, setData] = useState({});
  const host =
    window.location.hostname === "localhost"
      ? process.env.REACT_APP_FTP_HOST
      : window.location.hostname;
  const http = process.env.REACT_APP_FTP_SSL === true ? "https" : "http";
  const nas = process.env.REACT_APP_NAS_PATH;
  const ssid = process.env.REACT_APP_FTP_SHARE_SSID;
  const fid = process.env.REACT_APP_FTP_SHARE_SSID;
  const pwd = process.env.REACT_APP_FTP_SHARE_PWD;
  const url = `${http}://${host}${nas}?ssid=${ssid}&fid=${fid}`;
  window.localStorage.setItem("userData", JSON.stringify(props.data));

  useEffect(() => {
    setTimeout(() => {
      let data = window.localStorage.getItem("userData");
      setData(JSON.parse(data));
    }, 500);
  }, []);
  return (
    <Fragment>
      <div className={style.card}>
        <Card
          hoverable
          // cover={
          //   <img
          //     alt="cat"
          //     src={`${url}&path=/share/cat_01.jpg&openfolder=normal&ep=${pwd}`}
          //   />
          // }
          actions={[
            <SettingOutlined key="setting" />,
            <EditOutlined key="edit" />,
          ]}
        >
          <Meta
            // avatar={
            //   <Avatar
            //     src={`${url}&path=/share/cat_02.jpg&openfolder=normal&ep=${pwd}`}
            //   />
            // }
            title={data.name}
            description={data.role}
          />
        </Card>
      </div>
    </Fragment>
  );
};

export default UserProfile;
