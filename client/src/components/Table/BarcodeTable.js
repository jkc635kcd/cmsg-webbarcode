import React, { Fragment } from "react";
import { Button, Image, Table } from "antd";
import { UploadImage } from "../../api/UploadFile";
import { showDeleteConfirm } from "../Modal";
import { DeleteImage } from "../../api/FetchData";
import Barcode from "react-barcode";
import style from "./Table.module.css";

const BarcodeTable = (props) => {
  const host =
    window.location.hostname === "localhost"
      ? process.env.REACT_APP_FTP_HOST
      : window.location.hostname;
  const http = process.env.REACT_APP_FTP_SSL === true ? "https" : "http";
  const nas = process.env.REACT_APP_NAS_PATH;
  const ssid = process.env.REACT_APP_FTP_SHARE_SSID;
  const fid = process.env.REACT_APP_FTP_SHARE_SSID;
  const pwd = process.env.REACT_APP_FTP_SHARE_PWD;
  const url = `${http}://${host}${nas}?ssid=${ssid}&fid=${fid}`;
  const fadeImg = () => {
    let img = document.getElementById("item_image");
    if (img !== null) {
      img.children[0].style.animation = "none";
      setTimeout(() => {
        img.children[0].style.animation = null;
      }, 200);
    }
  };
  return (
    <Fragment>
      <Table
        id={"barcode_table"}
        className={style.barcode_table}
        columns={[
          {
            key: 1,
            title: "item",
            dataIndex: "item",
            align: "center",
            className: style.item,
          },
        ]}
        dataSource={[
          {
            item: (
              <Barcode
                format={"EAN13"}
                value={props.barcode}
                width={window.innerWidth > 1500 ? 3 : 3}
                height={40}
              />
            ),
          },
          {
            item: (
              <div
                style={{
                  display: window.innerWidth > 1600 ? "block" : "block",
                }}
              >
                {props.editable ? (
                  <div>
                    <Button
                      type={"primary"}
                      size={"large"}
                      danger
                      onClick={() =>
                        showDeleteConfirm(
                          "刪除圖片",
                          <div>是否刪除商品圖片 ?</div>,
                          DeleteImage,
                          props.barcode,
                          () => props.onDelete()
                        )
                      }
                    >
                      刪除圖片
                    </Button>
                  </div>
                ) : null}

                <div>
                  <Image
                    id={"item_image"}
                    // className={style.fade}
                    width={300}
                    height={300}
                    // src={`${url}&path=/${props.group_code}/${props.kind_code}/${
                    //   props.category_code
                    // }/image/${
                    //   props.barcode
                    // }.jpg&openfolder=normal&ep=${pwd}&time=${Date.now()}`}
                    src="error"
                    fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
                  />
                </div>

                {props.editable ? (
                  <div>
                    <UploadImage {...props} />
                  </div>
                ) : null}
                <div className={style.detail}>
                  <span style={{ color: "black" }}>
                    {`${props.spec_1_name}: ${props.spec_1}`}
                    <br />
                    {`${props.spec_2_name}: ${props.spec_2}`}
                    <br />
                    <span
                      style={{ color: "blue" }}
                    >{`價格: ${props.price}`}</span>
                  </span>
                  <span
                    style={{
                      display: props.href === "pivot_edit" ? "block" : "none",
                      color: "red",
                    }}
                  >{`折數: ${props.discount}`}</span>
                  <Button
                    type={"primary"}
                    onClick={() => props.setInfoDrawer(true)}
                  >
                    詳細資訊 / Q&A
                  </Button>
                </div>
              </div>
            ),
          },
        ]}
        pagination={false}
        bordered
        showHeader={false}
      />
    </Fragment>
  );
};

export default BarcodeTable;
