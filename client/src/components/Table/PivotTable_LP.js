import React, { Fragment } from "react";
import style from "./Table.module.css";
import { Button } from "antd";
import { CloseOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons";

const PivotTable = (props) => {
  //columnId生成
  const setColumnId = (i1, i2) => {
    return i1 + 1 >= 10
      ? i2 + 1 >= 10
        ? `${i1 + 1}${i2 + 1}`
        : `${i1 + 1}0${i2 + 1}`
      : i2 + 1 >= 10
      ? `0${i1 + 1}${i2 + 1}`
      : `0${i1 + 1}0${i2 + 1}`;
  };
  return (
    <Fragment>
      <table
        id={"PivotTable"}
        style={
          props.categories === "" || props.categories === undefined
            ? { display: "none" }
            : {
                display: window.innerWidth < 600 ? "grid" : "inline-table",
              }
        }
        className={style.pivotTable}
        key={1}
      >
        <thead>
          <tr>
            <th rowSpan="3" colSpan="1" className={style.spec_th2_title}>
              <div>
                <div>{`${props.spec_2_name}`}</div>
                <Button
                  shape="circle"
                  icon={<PlusOutlined />}
                  style={{ display: props.editable ? "" : "none" }}
                  size="large"
                  onClick={(e) =>
                    props.spec_2_onEdit(props.category_code, "add", e)
                  }
                />
              </div>
            </th>
            <th colSpan="8" className={style.spec_th1_title}>
              <div style={{ display: "inline-flex" }}>
                <div>{`${props.spec_1_name}`}</div>
                <Button
                  shape="circle"
                  icon={<PlusOutlined />}
                  style={{
                    display: props.editable ? "" : "none",
                    marginLeft: "10px",
                  }}
                  size="large"
                  onClick={(e) =>
                    props.spec_1_onEdit(props.category_code, "add", e)
                  }
                />
              </div>
            </th>
          </tr>
          <tr>
            {props.spec_1 === undefined
              ? null
              : [...Array.from(props.spec_1, (v, i) => v)].map((v1, i1) => (
                  <th
                    colSpan="1"
                    rowSpan="2"
                    id={`th1_${i1 + 1 < 10 ? "0" + (i1 + 1) : i1 + 1}`}
                    key={`th1_${i1 + 1 < 10 ? "0" + (i1 + 1) : i1 + 1}`}
                    className={style.spec_th1}
                  >
                    <Button
                      type="primary"
                      size="small"
                      danger
                      icon={<CloseOutlined />}
                      onClick={(e) =>
                        props.spec_1_onEdit(v1.spec_code, "remove", e)
                      }
                      style={{
                        display: props.editable ? "block" : "none",
                        position: "relative",
                        left: "0%",
                      }}
                    ></Button>
                    <button
                      className={style.spec_btn}
                      onClick={() => {
                        props.spec_1_onChange(v1.spec_code);
                        props.setSpec_1_var(v1.spec_var);
                        props.onClickSpec1([
                          `${i1 + 1 < 10 ? "0" + (i1 + 1) : i1 + 1}`,
                          v1.spec_var,
                        ]);
                      }}
                    >
                      {v1.spec_var +
                        (props.editable ? `(${v1.spec_code})` : "")}
                    </button>
                    <Button
                      type="primary"
                      size="small"
                      icon={<EditOutlined />}
                      style={{
                        display: props.editable ? "block" : "none",
                        position: "sticky",
                        left: "100%",
                      }}
                      onClick={(e) =>
                        props.spec_1_onEdit(v1.spec_code, "update", e)
                      }
                    ></Button>
                  </th>
                ))}
          </tr>
        </thead>
        <tbody key={1}>
          {props.spec_1 === undefined || props.spec_2 === undefined
            ? null
            : [...Array.from(props.spec_2, (v, i) => v)].map((v2, i2) => (
                <tr id={i2 + 1 >= 10 ? `${i2 + 1}` : `0${i2 + 1}`} key={i2}>
                  <th
                    colSpan="1"
                    rowSpan="1"
                    id={`th2_${i2 + 1 < 10 ? "0" + (i2 + 1) : i2 + 1}`}
                    className={style.spec_th2}
                  >
                    <Button
                      type="primary"
                      size="small"
                      danger
                      icon={<CloseOutlined />}
                      onClick={(e) =>
                        props.spec_2_onEdit(v2.spec_code, "remove", e)
                      }
                      style={{
                        display: props.editable ? "block" : "none",
                        position: "relative",
                        left: "0%",
                      }}
                    ></Button>
                    <button
                      className={style.spec_btn}
                      onClick={() => {
                        props.spec_2_onChange(v2.spec_code);
                        props.setSpec_2_var(v2.spec_var);
                        props.onClickSpec2([
                          `${i2 + 1 < 10 ? "0" + (i2 + 1) : i2 + 1}`,
                          v2.spec_var,
                        ]);
                      }}
                    >
                      {v2.spec_var +
                        (props.editable ? `(${v2.spec_code})` : "")}
                    </button>
                    <Button
                      type="primary"
                      size="small"
                      icon={<EditOutlined />}
                      style={{
                        display: props.editable ? "block" : "none",
                        position: "sticky",
                        left: "100%",
                      }}
                      onClick={(e) =>
                        props.spec_2_onEdit(v2.spec_code, "update", e)
                      }
                    ></Button>
                  </th>
                  {[...Array.from(props.spec_1, (v, i) => v)].map((v1, i1) => (
                    <td id={setColumnId(i1, i2)} key={i1}>
                      <div
                        id={`99${props.category_code}${v1.spec_code}${v2.spec_code}`}
                        contentEditable={props.editable}
                        // onFocus={(e) => props.setPrice(e.target.innerHTML)}
                        onBlur={(e) => {
                          props.price_onEdit(
                            `99${props.category_code}${v1.spec_code}${v2.spec_code}`,
                            e.target.innerHTML,
                            props.discount
                          );
                        }}
                        suppressContentEditableWarning={true}
                        key={i1}
                      >
                        {props.barcode_price[
                          `99${props.category_code}${v1.spec_code}${v2.spec_code}`
                        ] === undefined ||
                        props.barcode_price[
                          `99${props.category_code}${v1.spec_code}${v2.spec_code}`
                        ] === null
                          ? null
                          : Math.round(
                              props.barcode_price[
                                `99${props.category_code}${v1.spec_code}${v2.spec_code}`
                              ] * props.discount
                            )}
                      </div>
                    </td>
                  ))}
                </tr>
              ))}
        </tbody>
      </table>
    </Fragment>
  );
};

export default PivotTable;
