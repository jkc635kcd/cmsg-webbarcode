import React from "react";
import { Table } from "antd";
import { LastIndexContext } from "antd/lib/space";

const PivotTable_antd = () => {
  let sameKey;
  const value = [1, 2, 3, 4, 5, 6, 7];
  const columns = [
    {
      title: "寬度",
      dataIndex: "state_name",
      key: "state_name",
      render: (value, row, index) => {
        const obj = {
          children: value,
          props: {},
        };
        if (!(sameKey !== value)) {
          obj.props.rowSpan = 0;
          return obj;
        }
        const count = data.filter((item) => item.state_name === value).length;
        sameKey = value;
        obj.props.rowSpan = count;
        return obj;
      },
    },
    {
      title: "直徑",
      children: [
        {
          title: "3cm",
          dataIndex: "spec_3cm",
          key: 1,
          sorter: {
            compare: (a, b) => {
              console.log(a);
              console.log(b);
            },
          },
        },
        {
          title: "5cm",
          dataIndex: "spec_5cm",
          key: 2,
        },
        {
          title: "7cm",
          dataIndex: "spec_7cm",
          key: 3,
        },
        {
          title: "10cm",
          dataIndex: "spec_10cm",
          key: 4,
        },
        {
          title: "12cm",
          dataIndex: "spec_12cm",
          key: 5,
        },
        {
          title: "15cm",
          dataIndex: "spec_15cm",
          key: 6,
        },
        {
          title: "18cm",
          dataIndex: "spec_18cm",
          key: 7,
        },
      ],
    },
  ];

  const data = [
    {
      state_name: "5cm",
      spec_3cm: 35,
      spec_5cm: 55,
      spec_7cm: 75,
      spec_10cm: 105,
      spec_12cm: 125,
      spec_15cm: 155,
      spec_18cm: 185,
    },
    {
      state_name: "7cm",
      spec_3cm: 37,
      spec_5cm: 57,
      spec_7cm: 77,
      spec_10cm: 107,
      spec_12cm: 127,
      spec_15cm: 157,
      spec_18cm: 187,
    },
    {
      state_name: "10cm",
      spec_3cm: 310,
      spec_5cm: 510,
      spec_7cm: 710,
      spec_10cm: 1010,
      spec_12cm: 1210,
      spec_15cm: 1510,
      spec_18cm: 1810,
    },

    {
      state_name: "12cm",
      spec_3cm: 312,
      spec_5cm: 512,
      spec_7cm: 712,
      spec_10cm: 1012,
      spec_12cm: 1212,
      spec_15cm: 1512,
      spec_18cm: 1812,
    },
    {
      state_name: "15cm",
      spec_3cm: 315,
      spec_5cm: 515,
      spec_7cm: 715,
      spec_10cm: 1015,
      spec_12cm: 1215,
      spec_15cm: 1515,
      spec_18cm: 1815,
    },
  ];

  return (
    <Table
      columns={columns}
      dataSource={data.map((d, i) => {
        // d["spec_5cm"] += i;
        return { key: i, ...d };
      })}
      pagination={true}
      bordered={true}
      sticky={true}
    />
  );
};

export default PivotTable_antd;
