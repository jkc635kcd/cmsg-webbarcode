import React, { Fragment } from "react";
import { Drawer, Form, Input, Row, Col } from "antd";
import { EditOutlined } from "@ant-design/icons";
export const SettingTable = (props) => {
  const { Search } = Input;
  return (
    <Fragment>
      <Drawer
        title={props.title}
        width={window.innerWidth > 580 ? "400px" : "100%"}
        onClose={props.onClose}
        visible={props.visible}
        destroyOnClose={true}
      >
        <Form layout="vertical" initialValues={{ remember: true }}>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                label={`群組名稱`}
                name={`form_1`}
                rules={[{ required: true, message: `請輸入群組名稱!` }]}
              >
                <Search
                  placeholder={`群組名稱`}
                  enterButton={<EditOutlined />}
                  onSearch={(e) => {
                    props.onGroupUpdate(e);
                  }}
                  style={{ width: "100%" }}
                  defaultValue={props.default.group}
                  maxLength={10}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                label={`類別名稱`}
                name={`form_2`}
                rules={[{ required: true, message: `請輸入類別名稱!` }]}
              >
                <Search
                  placeholder={`類別名稱`}
                  enterButton={<EditOutlined />}
                  onSearch={props.onKindUpdate}
                  style={{ width: "100%" }}
                  defaultValue={props.default.kind}
                  maxLength={10}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                label={`種類名稱`}
                name={`form_3`}
                rules={[{ required: true, message: `請輸入種類名稱!` }]}
              >
                <Search
                  placeholder={`種類名稱`}
                  enterButton={<EditOutlined />}
                  onSearch={props.onCategoryUpdate}
                  style={{ width: "100%" }}
                  defaultValue={props.default.category}
                  maxLength={10}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                label={`規格(一)名稱`}
                name={`form_4`}
                rules={[{ required: true, message: `請輸入規格(一)名稱!` }]}
              >
                <Search
                  placeholder={`規格(一)名稱`}
                  enterButton={<EditOutlined />}
                  onSearch={props.onSpec1Update}
                  style={{ width: "100%" }}
                  defaultValue={props.default.spec_1}
                  maxLength={6}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                label={`規格(二)名稱`}
                name={`form_5`}
                rules={[{ required: true, message: `請輸入規格(二)名稱!` }]}
              >
                <Search
                  placeholder={`規格(二)名稱`}
                  enterButton={<EditOutlined />}
                  onSearch={props.onSpec2Update}
                  style={{ width: "100%" }}
                  defaultValue={props.default.spec_2}
                  maxLength={6}
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Drawer>
    </Fragment>
  );
};
