import { notification } from "antd";

export const Notification = (type, message, description) => {
  switch (type) {
    case "success":
      notification.success({
        message: message,
        description: description,
        placement: "bottomRight",
      });
      break;
    case "error":
      notification.error({
        message: message,
        description: description,
        placement: "bottomRight",
      });
      break;
    case "info":
      notification.info({
        message: message,
        description: description,
        placement: "bottomRight",
      });
      break;
    case "warning":
      notification.warning({
        message: message,
        description: description,
        placement: "bottomRight",
      });
      break;
    default:
      notification.info({
        message: message,
        description: description,
        placement: "bottomRight",
      });
      break;
  }
};
