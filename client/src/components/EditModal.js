import React, { useState } from "react";
import { Modal, Form, InputNumber, Input } from "antd";
import { Fragment } from "react";

/**
 * @property {bool} visible
 * @property {string} title
 * @property {string} content
 * @property {string} type
 * @property {string} target
 * @property {void} addHook
 * @property {void} onCancel
 * @property {void} onCreate
 */
const EditModal = ({
  visible,
  title,
  content,
  type,
  action,
  target,
  addHook,
  onCancel,
  onCreate,
}) => {
  const [form] = Form.useForm();
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [cancelProps, setCancelProps] = useState(false);
  const typeName =
    type === "group"
      ? "群組"
      : type === "kind"
      ? "類別"
      : type === "category"
      ? "種類"
      : "規格";

  const formatCode = (type, code, target) => {
    switch (type) {
      case "group":
        return code;
      case "kind":
        return code < 10 ? `${target}0${code}` : `${target}${code}`;
      case "category":
        return code < 10
          ? `${target}00${code}`
          : code < 100
          ? `${target}0${code}`
          : `${target}${code}`;
      case "spec":
        return code < 10 ? `0${code}` : code;
    }
  };

  const handleEdit = () => {
    form
      .validateFields()
      .then((values) => {
        form.resetFields();
        setConfirmLoading(true);
        setCancelProps(true);
        return new Promise((resolve, reject) => {
          setConfirmLoading(false);
          setCancelProps(false);
          const res = handleHook(type, addHook, target, values).then((e) => {
            if (e.status === 200) {
              onCreate(true);
              if (action === "update") {
                console.log("update data success.");
              } else {
                console.log("add new data success.");
              }
            }
          });
          resolve(res);
        }).catch((err) => {
          console.log(err);
          if (action === "update") {
            console.log("failed to update data!");
          } else {
            console.log("failed to add new data!");
          }

          onCreate(true);
        });
      })
      .catch((info) => {
        console.log("Validate Failed:", info);
      });
  };

  // 處理修改事件
  const handleHook = (type, hook, target, values) => {
    if (action === "update" && type !== "category") {
      return hook(values.form_1);
    }
    switch (type) {
      case "group":
        return hook(values.form_1, values.form_2);
      case "kind":
        return hook(
          formatCode(type, values.form_1, target).toString(),
          values.form_2,
          target
        );
      case "category":
        return hook(
          formatCode(type, values.form_1, target).toString(),
          values.form_2,
          values.form_3,
          values.form_4
        );
      case "spec":
        return hook(
          formatCode(type, values.form_1, target).toString(),
          values.form_2,
          target
        );
    }
  };
  return (
    <Fragment>
      <Modal
        visible={visible}
        title={title}
        okText={"確認"}
        cancelText={"取消"}
        onCancel={onCancel}
        confirmLoading={confirmLoading}
        cancelButtonProps={{ disabled: cancelProps }}
        destroyOnClose={true}
        maskClosable={!cancelProps}
        closable={!cancelProps}
        afterClose={() => form.resetFields()}
        onOk={() => handleEdit()}
      >
        {action === "update" && type !== "category" ? (
          <Form
            form={form}
            layout={{ labelCol: { span: 8 }, wrapperCol: { span: 16 } }}
            initialValues={{ remember: true }}
            onFinish={(e) => console.log(e)}
            onFinishFailed={(e) => console.log(e)}
          >
            <div>{content}</div>
            <br />
            <Form.Item
              label={`${typeName}名稱`}
              name={`form_1`}
              rules={[{ required: true, message: `請輸入${typeName}名稱!` }]}
            >
              <Input
                maxLength={10}
                placeholder={`${typeName}名稱`}
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    handleEdit();
                  }
                }}
                autoFocus={true}
              />
            </Form.Item>
          </Form>
        ) : type === "category" ? (
          <Form
            form={form}
            layout={{ labelCol: { span: 8 }, wrapperCol: { span: 16 } }}
            initialValues={{ remember: true }}
          >
            <div>{content}</div>
            <br />
            <Form.Item
              label={`${typeName}代號`}
              name={`form_1`}
              rules={[{ required: true, message: "請輸入代號!" }]}
            >
              <InputNumber
                min={type === "group" ? 0 : 1}
                max={
                  type === "group" ? 9 : type === "kind" || "spec" ? 99 : 999
                }
                placeholder={`${typeName}代號`}
                autoFocus={true}
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    handleEdit();
                  }
                }}
              />
            </Form.Item>
            <Form.Item
              label={`${typeName}名稱`}
              name={`form_2`}
              rules={[{ required: true, message: `請輸入${typeName}名稱!` }]}
            >
              <Input
                maxLength={10}
                placeholder={`${typeName}名稱`}
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    handleEdit();
                  }
                }}
              />
            </Form.Item>
            {/* 新增種類規格名稱 */}
            <Form.Item
              label={`${typeName}規格名稱(欄)`}
              name={`form_3`}
              rules={[
                { required: true, message: `請輸入${typeName}規格名稱(欄)!` },
              ]}
            >
              <Input
                maxLength={10}
                placeholder={`${typeName}規格名稱(欄)`}
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    handleEdit();
                  }
                }}
              />
            </Form.Item>
            <Form.Item
              label={`${typeName}規格名稱(列)`}
              name={`form_4`}
              rules={[
                { required: true, message: `請輸入${typeName}規格名稱(列)!` },
              ]}
            >
              <Input
                maxLength={10}
                placeholder={`${typeName}規格名稱(列)`}
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    handleEdit();
                  }
                }}
              />
            </Form.Item>
          </Form>
        ) : (
          <Form
            form={form}
            layout={{ labelCol: { span: 8 }, wrapperCol: { span: 16 } }}
            initialValues={{ remember: true }}
            onFinish={(e) => console.log(e)}
            onFinishFailed={(e) => console.log(e)}
          >
            <div>{content}</div>
            <br />
            <Form.Item
              label={`${typeName}代號`}
              name={`form_1`}
              rules={[{ required: true, message: "請輸入代號!" }]}
            >
              <InputNumber
                min={type === "group" ? 0 : 1}
                max={type === "group" ? 9 : type == "kind" || "spec" ? 99 : 999}
                placeholder={`${typeName}代號`}
                autoFocus={true}
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    handleEdit();
                  }
                }}
              />
            </Form.Item>
            <Form.Item
              label={`${typeName}名稱`}
              name={`form_2`}
              rules={[{ required: true, message: `請輸入${typeName}名稱!` }]}
            >
              <Input
                maxLength={10}
                placeholder={`${typeName}名稱`}
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    handleEdit();
                  }
                }}
              />
            </Form.Item>
          </Form>
        )}
      </Modal>
    </Fragment>
  );
};

export default EditModal;
