import axios from "axios";
const protocol = window.location.protocol;
const version = process.env.REACT_APP_API_VERSION;
const API_URL = `${protocol}//${window.location.hostname}/api${version}/verify/`;

const getPublic = () => {
  return axios.get(API_URL + "all");
};

const getUser = () => {
  return axios.get(API_URL + "user", { withCredentials: true });
};

const getModerator = () => {
  return axios.get(API_URL + "moderator", { withCredentials: true });
};

const getAdmin = () => {
  return axios.get(API_URL + "admin", { withCredentials: true });
};

export default {
  getPublic,
  getUser,
  getModerator,
  getAdmin,
};
