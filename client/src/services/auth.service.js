import axios from "axios";
const protocol = window.location.protocol;
const version = process.env.REACT_APP_API_VERSION;
const API_URL = `${protocol}//${window.location.hostname}/api${version}/auth/`;

export const login = (username, password) => {
  try {
    return axios
      .post(
        API_URL + "login",
        {
          username,
          password,
        },
        {
          withCredentials: true,
        }
      )
      .then((response) => {
        axios.post(
          API_URL + "setcookie",
          { username },
          { withCredentials: true }
        );
        return response;
      })
      .catch((error) => {
        if (error.response) {
          return error.response;
        }
      });
  } catch (error) {
    console.log(error);
  }
};

export const logout = () => {
  try {
    return axios
      .post(API_URL + "logout", {}, { withCredentials: true })
      .then((response) => {
        return response;
      });
  } catch (error) {
    console.log(error);
  }
};

export const getCurrentUser = () => {
  axios.defaults.withCredentials = true;
  return axios({
    method: "get",
    baseURL: API_URL,
    url: "profile",
    withCredentials: true,
  });
};
