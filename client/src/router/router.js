import React, { Component, useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { Home, Login, BarcodeBase, Profile } from "./pages";
import { Layout, Menu } from "antd";
import Navbar from "../components/Navbar/Navbar-ant";
import { getCurrentUser, logout } from "../services/auth.service";
import { LoginOutlined } from "@ant-design/icons";
// import { BarcodeBase } from "./pages/BarcodeBase";

const { Header, Content } = Layout;

const AdminRoute = ({ component: Component, authed, ...rest }) => {
  const [content, setContent] = useState({ status: 200 });
  useEffect(() => {
    const fetchData = async () => {
      await getCurrentUser()
        .then((res) => {
          console.log(res);
          setContent(res);
        })
        .catch((error) => {
          console.log(error);
          setContent(error);
        });
    };
    fetchData();
  }, []);

  return (
    <Route
      {...rest}
      render={(props) =>
        content.status === 200 ? (
          <Component {...content} />
        ) : (
          <Redirect
            to={{ pathname: "/login", state: { from: props.location } }}
          />
        )
      }
    />
  );
};

const LogoutRoute = () => {
  return (
    <Route
      render={(props) =>
        handleLogout() ? (
          <Redirect
            to={{ pathname: "/login", state: { from: props.location } }}
          />
        ) : (
          <Redirect
            to={{ pathname: "/profile", state: { from: props.location } }}
          />
        )
      }
    />
  );
};

const handleLogout = () => {
  let result = window.confirm("是否登出系統?");
  if (result) {
    logout()
      .then((res) => console.log(res))
      .catch((error) => {
        console.log(error);
      });
  }
  return result;
};

export default function webRouter() {
  return (
    <Layout>
      <Navbar />
      <Content style={{ padding: "0 20px", marginTop: 36 }}>
        <div style={{ minHeight: 500 }}>
          <Router>
            <Switch>
              <Redirect exact from="/" to="/home" />
              <Route exact path={["/home"]} component={Home} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/barcode" component={BarcodeBase} />
              <LogoutRoute path="/logout" />
              <AdminRoute authed={200} path="/profile" component={Profile} />
            </Switch>
          </Router>
        </div>
      </Content>
    </Layout>
  );
}
