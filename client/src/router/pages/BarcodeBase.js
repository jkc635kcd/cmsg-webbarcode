import React, { Fragment, lazy, Suspense } from "react";
import { Spin } from "antd";
import { fetchProductData } from "../../api/PrefetchApi";
const PivotTableUI = lazy(() =>
  import("../../components/PivotTableUI/PivotTableUI")
);
// fetch data befor render
let resource;
if (window.location.pathname === "/barcode") {
  resource = fetchProductData();
}

const PivotTable = () => {
  const d = resource.product.read();
  console.log(d);
  return <PivotTableUI data={d} />;
};

const BarcodeBase = () => {
  return (
    <Fragment>
      <Suspense
        fallback={
          <Spin
            spinning={true}
            style={{ position: "fixed", top: "50%", left: "50%" }}
            size={"large"}
          />
        }
      >
        <PivotTable />
      </Suspense>
    </Fragment>
  );
};

export default BarcodeBase;
