import React, { Fragment, lazy, Suspense, useEffect } from "react";
import { Spin } from "antd";
import UserProfile from "../../components/UserProfile/UserProfile";

const Profile = (props) => {
  return (
    <Fragment>
      <UserProfile {...props} />
    </Fragment>
  );
};

export default Profile;
