import React, { Fragment, lazy, Suspense } from "react";
// import TestPage from "../../components/TestPage";
const TestPage = lazy(() => import("../../components/TestPage"));

const renderLoader = () => <p>Loading</p>;

const Test = () => {
  return (
    <Fragment>
      <Suspense fallback={renderLoader}>
        <TestPage />
      </Suspense>
    </Fragment>
  );
};

export default Test;
