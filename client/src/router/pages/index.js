export { default as Home } from "./Home";
export { default as BarcodeBase } from "./BarcodeBase";
export { default as Login } from "./Login";
export { default as Profile } from "./Profile";
