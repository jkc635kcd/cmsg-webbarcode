import React, { Fragment, useEffect, useState } from "react";
import { Form, Input, Button, Card } from "antd";
import {
  UserOutlined,
  LockOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
} from "@ant-design/icons";
import { login, getCurrentUser } from "../../services/auth.service";
import axios from "axios";

const http = process.env.REACT_APP_API_SSL == true ? "https" : "http";
const api = process.env.REACT_APP_API_VERSION;

const Login = () => {
  const onFinish = async (values) => {
    let res = await login(values.username, values.password);
    console.log(res);
    if (res.status === 200) {
      alert(res.data);
      window.location.href = "profile";
    } else {
      alert(res.data.message);
    }
  };

  useEffect(() => {
    getCurrentUser()
      .then((res) => {
        window.location.href = "profile";
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  // useEffect(() => {
  //   axios.defaults.withCredentials = true;
  //   const getCsrfToken = async () => {
  //     const { data } = await axios.get(
  //       `${http}://${window.location.hostname}${api}/auth/csrf`
  //     );
  //     axios.defaults.headers["x-csrf-token"] = data.csrfToken;
  //   };
  //   getCsrfToken();
  //   console.log("get csrf");
  // }, []);

  return (
    <Fragment>
      <Card
        title={"系統登入"}
        bordered={true}
        hoverable
        style={{
          width: "350px",
          height: "250px",
          margin: "auto",
          textAlign: "center",
        }}
      >
        <Form
          name="sysLogin"
          initialValues={{ remember: true }}
          onFinish={onFinish}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: "請輸入使用者帳號!" }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="帳號"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "請輸入使用者密碼!" }]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              placeholder="密碼"
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              登入
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </Fragment>
  );
};
export default Login;
